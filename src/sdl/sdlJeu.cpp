#include <cassert>
#include <time.h>
#include "sdlJeu.h"
#include <stdlib.h>
#include <math.h>

#include <iostream>
using namespace std;

const int TAILLE_SPRITE = 10;

float temps () {
    return float(SDL_GetTicks()) / CLOCKS_PER_SEC;  // conversion des ms en secondes en divisant par 1000
}

// ============= CLASS IMAGE =============== //

Image::Image () {
    surface = NULL;
    texture = NULL;
    has_changed = false;
}

void Image::loadFromFile (const char* filename, SDL_Renderer * renderer) {
    surface = IMG_Load(filename);
    if (surface == NULL) {
        string nfn = string("../") + filename;
        cout << "Error: cannot load "<< filename <<". Trying "<<nfn<<endl;
        surface = IMG_Load(nfn.c_str());
        if (surface == NULL) {
            nfn = string("../") + nfn;
            surface = IMG_Load(nfn.c_str());
        }
    }
    if (surface == NULL) {
        cout<<"Error: cannot load "<< filename <<endl;
        SDL_Quit();
        exit(1);
    }

    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(surface,SDL_PIXELFORMAT_ARGB8888,0);
    SDL_FreeSurface(surface);
    surface = surfaceCorrectPixelFormat;

    texture = SDL_CreateTextureFromSurface(renderer,surfaceCorrectPixelFormat);
    if (texture == NULL) {
        cout << "Error: problem to create the texture of "<< filename<< endl;
        SDL_Quit();
        exit(1);
    }
}

void Image::loadFromCurrentSurface (SDL_Renderer * renderer) {
    texture = SDL_CreateTextureFromSurface(renderer,surface);
    if (texture == NULL) {
        cout << "Error: problem to create the texture from surface " << endl;
        SDL_Quit();
        exit(1);
    }
}

void Image::draw (SDL_Renderer * renderer, int x, int y, int w, int h) {
    int ok;
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = (w<0)?surface->w:w;
    r.h = (h<0)?surface->h:h;

    if (has_changed) {
        ok = SDL_UpdateTexture(texture,NULL,surface->pixels,surface->pitch);
        assert(ok == 0);
        has_changed = false;
    }

    ok = SDL_RenderCopy(renderer,texture,NULL,&r);
    assert(ok == 0);
}

SDL_Texture * Image::getTexture() const {return texture;}

void Image::setSurface(SDL_Surface * surf) {surface = surf;}










// ============= CLASS SDLJEU =============== //

sdlJeu::sdlJeu () : jeu(1) {
    // Initialisation de la SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    if (TTF_Init() != 0) {
        cout << "Erreur lors de l'initialisation de la SDL_ttf : " << TTF_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG; // !!! DANS CES 6 LIGNES : QUELQUE CHOSE A CHANGER (selon extensions des fichiers images) ? !!!
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
    {
        cout << "SDL_mixer could not initialize! SDL_mixer Error: " << Mix_GetError() << endl;
        cout << "No sound !!!" << endl;
        //SDL_Quit();exit(1); // !!! DECOMMENTER ? !!!
        withSound = false;
    }
    else withSound = true;

	int dimx, dimy;
	dimx = jeu.getConstTerrain().getDimX();
	dimy = jeu.getConstTerrain().getDimY();
	dimx = dimx * TAILLE_SPRITE + 28;
	dimy = dimy * TAILLE_SPRITE + 17;

    // Creation de la fenetre
    window = SDL_CreateWindow("Projet Code Lyoko", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, dimx, dimy, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED); // !!! SDL_RENDERER_ACCELERATED : à CREUSER POUR REDUIRE LAGUE ?

    // IMAGES
    im_heros_vers_haut.loadFromFile("data/heros/heros_vers_haut.png",renderer);
    im_heros_vers_bas.loadFromFile("data/heros/heros_vers_bas.png",renderer);
    im_heros_vers_gauche.loadFromFile("data/heros/heros_vers_gauche.png",renderer);
    im_heros_vers_droite.loadFromFile("data/heros/heros_vers_droite.png",renderer);
    im_mur.loadFromFile("data/mur.jpg",renderer);
    im_sol.loadFromFile("data/sol.jpeg",renderer);
    im_sol2.loadFromFile("data/sol2.jpeg",renderer);
    im_fleche_vers_haut.loadFromFile("data/fleche/fleche_vers_haut.png",renderer);
    im_fleche_vers_bas.loadFromFile("data/fleche/fleche_vers_bas.png",renderer);
    im_fleche_vers_gauche.loadFromFile("data/fleche/fleche_vers_gauche.png",renderer);
    im_fleche_vers_droite.loadFromFile("data/fleche/fleche_vers_droite.png",renderer);
    im_troll_vers_haut.loadFromFile("data/troll/troll_vers_haut.png",renderer);
    im_troll_vers_bas.loadFromFile("data/troll/troll_vers_bas.png",renderer);
    im_troll_vers_gauche.loadFromFile("data/troll/troll_vers_gauche.png",renderer);
    im_troll_vers_droite.loadFromFile("data/troll/troll_vers_droite.png",renderer);
    im_gobelin_vers_gauche.loadFromFile("data/gobelin/gobelin_vers_gauche.png",renderer);
    im_gobelin_vers_droite.loadFromFile("data/gobelin/gobelin_vers_droite.png",renderer);
    im_epee.loadFromFile("data/epee.gif",renderer);
    im_massue.loadFromFile("data/massue.png",renderer);
    im_petite_massue.loadFromFile("data/petite_massue.png",renderer);
    im_menu.loadFromFile("data/menu.jpg",renderer);
    im_tireur_vers_gauche.loadFromFile("data/tireur/tireur_vers_gauche.png", renderer);
    im_tireur_vers_droite.loadFromFile("data/tireur/tireur_vers_droite.png", renderer);
    im_feu.loadFromFile("data/feu/feu.png", renderer);
    im_map.loadFromFile("data/map.png", renderer);

    // FONTS
    font = TTF_OpenFont("data/DejaVuSansCondensed.ttf",50);
    if (font == NULL)
        font = TTF_OpenFont("../data/DejaVuSansCondensed.ttf",50);
    if (font == NULL) {
            cout << "Failed to load DejaVuSansCondensed.ttf! SDL_TTF Error: " << TTF_GetError() << endl;
            SDL_Quit();
            exit(1);
	}
    font_2 = TTF_OpenFont("data/police_titre_jeu.ttf",50);
    if (font_2 == NULL)
        font_2 = TTF_OpenFont("../data/police_titre_jeu.ttf",50);
    if (font_2 == NULL) {
            cout << "Failed to load police_titre_jeu.ttf! SDL_TTF Error: " << TTF_GetError() << endl;
            SDL_Quit();
            exit(1);
	}
	//font_color.r = 50;font_color.g = 50;font_color.b = 255;
    font_color_2.r = 0;font_color_2.g = 0;font_color_2.b = 0;
	font_im.setSurface(TTF_RenderText_Solid(font,"Projet Code Lyoko",font_color));
	font_im.loadFromCurrentSurface(renderer);
    font_color.r = 255; font_color.g=255; font_color.b = 255;
    font_jouer.setSurface(TTF_RenderText_Solid(font,"Jouer une partie", font_color));
    font_jouer.loadFromCurrentSurface(renderer);
    font_quitter.setSurface(TTF_RenderText_Solid(font,"Quitter le jeu", font_color));
    font_quitter.loadFromCurrentSurface(renderer);
    font_reprendre.setSurface(TTF_RenderText_Solid(font,"Reprendre la partie", font_color));
    font_reprendre.loadFromCurrentSurface(renderer);
    font_retourMenu.setSurface(TTF_RenderText_Solid(font,"Retourner au menu", font_color));
    font_retourMenu.loadFromCurrentSurface(renderer);
    font_titre_du_jeu.setSurface(TTF_RenderText_Solid(font_2,"Projet Code Lyoko", font_color_2));
    font_titre_du_jeu.loadFromCurrentSurface(renderer);

    // SONS
    if (withSound)
    {

        sound = Mix_LoadMUS("data/musique.mp3");
        coupEpee = Mix_LoadWAV("data/coupEpee.wav");
        tirArc = Mix_LoadWAV("data/arc.wav");
        tirFeu = Mix_LoadWAV("data/tirFeu.wav");
        impactFeu = Mix_LoadWAV("data/impactFeu.wav");
        impactEpeeSurMonstre = Mix_LoadWAV("data/impactEpeeSurMonstre.wav");
        impactFlecheSurMonstre = Mix_LoadWAV("data/impactFlecheSurMonstre.wav");
        impactHeros = Mix_LoadWAV("data/impactHeros.wav");
        coupMassueTroll = Mix_LoadWAV("data/coupMassueTroll.wav");
        coupMassueGobelin = Mix_LoadWAV("data/coupMassueGobelin.wav");


        if (sound == NULL)
            sound = Mix_LoadMUS("../data/musique.mp3");
        if (sound == NULL) {
                cout << "Failed to load musique.mp3! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeMusic(40);


        if (coupEpee == NULL)
            coupEpee = Mix_LoadWAV("../data/coupEpee.wav");
        if (coupEpee == NULL) {
                cout << "Failed to load epee.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(coupEpee, 60);


        if (tirArc == NULL)
            tirArc = Mix_LoadWAV("../data/arc.wav");
        if (tirArc == NULL) {
                cout << "Failed to load arc.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(tirArc, 30);


        if (tirFeu == NULL)
        tirFeu = Mix_LoadWAV("../data/tirFeu.wav");
        if (tirFeu == NULL) {
                cout << "Failed to load tirFeu.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(tirFeu, 128);


        /*if (impactFeu == NULL)
            impactFeu = Mix_LoadWAV("../data/impactFeu.wav");
        if (impactFeu == NULL) {
                cout << "Failed to load impactFeu.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(impactFeu, 30);*/


        if (impactEpeeSurMonstre == NULL)
            impactEpeeSurMonstre = Mix_LoadWAV("../data/impactEpeeSurMonstre.wav");
        if (impactEpeeSurMonstre == NULL) {
                cout << "Failed to load impactEpeeSurMonstre.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(impactEpeeSurMonstre, 70);


        if (impactFlecheSurMonstre == NULL)
            impactFlecheSurMonstre = Mix_LoadWAV("../data/impactFlecheSurMonstre.wav");
        if (impactFlecheSurMonstre == NULL) {
                cout << "Failed to load impactFlecheSurMonstre.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(impactFlecheSurMonstre, 70);


        if (impactHeros == NULL)
            impactHeros = Mix_LoadWAV("../data/impactHeros.wav");
        if (impactHeros == NULL) {
                cout << "Failed to load impactHeros.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(impactHeros, 80);


        /*if (coupMassueTroll == NULL)
            coupMassueTroll = Mix_LoadWAV("../data/coupMassueTroll.wav");
        if (coupMassueTroll == NULL) {
                cout << "Failed to load coupMassueTroll.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(coupMassueTroll, 30);


        if (coupMassueGobelin == NULL)
            coupMassueGobelin = Mix_LoadWAV("../data/coupMassueGobelin.wav");
        if (coupMassueGobelin == NULL) {
                cout << "Failed to load coupMassueGobelin.wav! SDL_mixer Error: " << Mix_GetError() << endl;
                SDL_Quit();
                exit(1);
        }
        Mix_VolumeChunk(coupMassueGobelin, 30);*/
    }

    
}

sdlJeu::~sdlJeu () { // !!! Il ne manque pas des quit (cf diapo 41 CM6) ? Notamment procédures SDL_DestroyTexture (cf diapo 43 CM6), IMG_Quit() et MIX_CloseAudio() (cf diapo 41 CM6)
    if (withSound) Mix_Quit();
    TTF_CloseFont(font); // A ENLEVER ?
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void sdlJeu::sdlAff () {
	//Remplir l'écran d'une couleur donnée
    //SDL_SetRenderDrawColor(renderer, 105, 100, 89, 255);
    SDL_RenderClear(renderer);

	int x,y;
	const Terrain& ter = jeu.getConstTerrain();
	const Heros& her = jeu.getConstHeros();
	const Vague& vag = jeu.getConstVague();
	/*const Troll& tro = jeu.getConstTroll();
    const Gobelin& gob = jeu.getConstGobelin();*/
    const Fleche* fle = NULL;
    const BouleDeFeu* boule1 = NULL;
    const BouleDeFeu* boule2 = NULL;
    const BouleDeFeu* boule3 = NULL;
    if (her.getFleche() != NULL) {
        fle = her.getFleche();
    }

    //Afficher les sprites du sol
    //im_sol.draw(renderer, 40, 40, 1500, 641);
    /*im_sol.draw(renderer, 40, 40, 750, 321);
    im_sol.draw(renderer, 790, 40, 750, 321);
    im_sol.draw(renderer, 40, 361, 750, 320);
    im_sol.draw(renderer, 790, 361, 750, 320);*/
    //int l = 0;
    //int m = 0;
    /*for (l=0;l<6;++l)
    {
        for (m=0;m<2;++m)
        {
            im_sol.draw(renderer, 40+250*l, 40+257*m, 250, 257);
        }
        im_sol2.draw(renderer, 40+250*l, 40+257*m, 250, 127);
    }*/

    im_map.draw(renderer, 0, 0, 1576, 717);

    // Afficher les sprites des murs, du sol, de l'épée si le joueur donne un coup d'épée ("de l'épée si le joueur donne un coup d'épée" : TEMPORAIRE DANS LA CONCEPTION DU JEU), de la massue du troll si un troll donne un coup de massue ("de la massue du troll si un troll donne un coup de massue" : TEMPORAIRE DANS LA CONCEPTION DU JEU), et de la massue du gobelin si un gobelin donne un coup de massue ("et de la massue du gobelin si un gobelin donne un coup de massue" : TEMPORAIRE DANS LA CONCEPTION DU JEU)
    int i = 0;
    int j = 0;
    for (x=0;x<ter.getDimX();++x)
    {

            for (y=0;y<ter.getDimY();++y)
            {
                //if ((ter.getXY(x,y)=='#'))
                    //im_mur.draw(renderer,x*TAILLE_SPRITE,y*TAILLE_SPRITE,TAILLE_SPRITE,TAILLE_SPRITE);
                //if (ter.getXY(x,y)==' ')
                    //im_sol.draw(renderer,x*TAILLE_SPRITE,y*TAILLE_SPRITE,TAILLE_SPRITE,TAILLE_SPRITE);
                if (ter.getXY(x,y)=='a')
                    im_epee.draw(renderer,x*TAILLE_SPRITE,y*TAILLE_SPRITE,TAILLE_SPRITE,TAILLE_SPRITE);
                if (ter.getXY(x,y)=='o')
                    im_massue.draw(renderer,x*TAILLE_SPRITE,y*TAILLE_SPRITE,TAILLE_SPRITE,TAILLE_SPRITE);
                if (ter.getXY(x,y)=='k')
                    im_petite_massue.draw(renderer,x*TAILLE_SPRITE,y*TAILLE_SPRITE,TAILLE_SPRITE,TAILLE_SPRITE);

                i++;
            }

        j++;
    }

	// Afficher le sprite du Héros
	if(her.enVie()) {

        string direction = her.getDirectionVisee ();
        int direction2 = 0;
        if (direction == "haut") direction2 = 1;
        if (direction == "bas") direction2 = 2;
        if (direction == "gauche") direction2 = 3;
        if (direction == "droite") direction2 = 4;
        switch (direction2) {

            case 1:
                im_heros_vers_bas.draw(renderer,her.getX()*TAILLE_SPRITE,her.getY()*TAILLE_SPRITE,her.getLargeur()*TAILLE_SPRITE,her.getHauteur()*TAILLE_SPRITE);
				break;

            case 2:
                im_heros_vers_haut.draw(renderer,her.getX()*TAILLE_SPRITE,her.getY()*TAILLE_SPRITE,her.getLargeur()*TAILLE_SPRITE,her.getHauteur()*TAILLE_SPRITE);
				break;

            case 3:
                im_heros_vers_gauche.draw(renderer,her.getX()*TAILLE_SPRITE,her.getY()*TAILLE_SPRITE,her.getLargeur()*TAILLE_SPRITE,her.getHauteur()*TAILLE_SPRITE);
				break;

            case 4:
                im_heros_vers_droite.draw(renderer,her.getX()*TAILLE_SPRITE,her.getY()*TAILLE_SPRITE,her.getLargeur()*TAILLE_SPRITE,her.getHauteur()*TAILLE_SPRITE);
				break;

            default: break;
        }

    }

	// Afficher les sprites des Trolls
	for(int i=0; i<3;i++)
	{
        if(vag.getTabTroll()[i].enVie()) {

            string direction = vag.getTabTroll()[i].getDirectionVisee ();
            int direction2 = 0;
            if (direction == "haut") direction2 = 1;
            if (direction == "bas") direction2 = 2;
            if (direction == "gauche") direction2 = 3;
            if (direction == "droite") direction2 = 4;
            switch (direction2) {

                case 1:
                    im_troll_vers_bas.draw(renderer,vag.getTabTroll()[i].getX()*TAILLE_SPRITE,vag.getTabTroll()[i].getY()*TAILLE_SPRITE,vag.getTabTroll()[i].getLargeur()*TAILLE_SPRITE,vag.getTabTroll()[i].getHauteur()*TAILLE_SPRITE);
                    break;

                case 2:
                    im_troll_vers_haut.draw(renderer,vag.getTabTroll()[i].getX()*TAILLE_SPRITE,vag.getTabTroll()[i].getY()*TAILLE_SPRITE,vag.getTabTroll()[i].getLargeur()*TAILLE_SPRITE, vag.getTabTroll()[i].getHauteur()*TAILLE_SPRITE);
                    break;

                case 3:
                    im_troll_vers_gauche.draw(renderer,vag.getTabTroll()[i].getX()*TAILLE_SPRITE,vag.getTabTroll()[i].getY()*TAILLE_SPRITE,vag.getTabTroll()[i].getLargeur()*TAILLE_SPRITE, vag.getTabTroll()[i].getHauteur()*TAILLE_SPRITE);
                    break;

                case 4:
                    im_troll_vers_droite.draw(renderer,vag.getTabTroll()[i].getX()*TAILLE_SPRITE,vag.getTabTroll()[i].getY()*TAILLE_SPRITE,vag.getTabTroll()[i].getLargeur()*TAILLE_SPRITE, vag.getTabTroll()[i].getHauteur()*TAILLE_SPRITE);
                    break;

                default: break;
            }
        }
    }


    // Afficher les sprites des Gobelins
    for(int i=0; i<3; i++)
    {
        if(vag.getTabGobelin()[i].enVie()) {

            string direction = vag.getTabGobelin()[i].getDirectionVisee ();
            int direction2 = 0;
            if (direction == "gauche") direction2 = 3;
            if (direction == "droite") direction2 = 4;
            switch (direction2) {

                case 3:
                    im_gobelin_vers_gauche.draw(renderer,vag.getTabGobelin()[i].getX()*TAILLE_SPRITE,vag.getTabGobelin()[i].getY()*TAILLE_SPRITE,vag.getTabGobelin()[i].getLargeur()*TAILLE_SPRITE,vag.getTabGobelin()[i].getHauteur()*TAILLE_SPRITE);
                    break;

                case 4:
                    im_gobelin_vers_droite.draw(renderer,vag.getTabGobelin()[i].getX()*TAILLE_SPRITE,vag.getTabGobelin()[i].getY()*TAILLE_SPRITE,vag.getTabGobelin()[i].getLargeur()*TAILLE_SPRITE,vag.getTabGobelin()[i].getHauteur()*TAILLE_SPRITE);
                    break;

                default: break;
            }
        }

    }

    // Afficher les sprites des Tireurs et des Boules de feu
    for(int i=0; i<3;i++)
    {

        if (vag.getTabTireur()[i].getBouleDeFeu1() != NULL) {
        boule1 = vag.getTabTireur()[i].getBouleDeFeu1();}

        if (vag.getTabTireur()[i].getBouleDeFeu2() != NULL) {
        boule2 = vag.getTabTireur()[i].getBouleDeFeu2();}

        if (vag.getTabTireur()[i].getBouleDeFeu3() != NULL) {
        boule3 = vag.getTabTireur()[i].getBouleDeFeu3();}



        if(vag.getTabTireur()[i].enVie())
        {
            int direction = vag.getTabTireur()[i].getDir ();
            int direction2 = 0;
            if ((direction == 0) || (direction == 1)) direction2 = 0;
            if ((direction == 2) || (direction == 3)) direction2 = 1;

            switch (direction2) {

                case 0:
                    im_tireur_vers_droite.draw(renderer, vag.getTabTireur()[i].getX()*TAILLE_SPRITE, vag.getTabTireur()[i].getY()*TAILLE_SPRITE, vag.getTabTireur()[i].getLargeur()*TAILLE_SPRITE, vag.getTabTireur()[i].getHauteur()*TAILLE_SPRITE);
                    break;

                case 1:
                    im_tireur_vers_gauche.draw(renderer, vag.getTabTireur()[i].getX()*TAILLE_SPRITE, vag.getTabTireur()[i].getY()*TAILLE_SPRITE, vag.getTabTireur()[i].getLargeur()*TAILLE_SPRITE, vag.getTabTireur()[i].getHauteur()*TAILLE_SPRITE);
                    break;

                default: break;

            }

        }


        if(vag.getTabTireur()[i].getBouleDeFeu1() != NULL && vag.getTabTireur()[i].getBouleDeFeu1()->getEnMouvement())
        {
            im_feu.draw(renderer, boule1->getX()*TAILLE_SPRITE, boule1->getY()*TAILLE_SPRITE, boule1->getDimx()*TAILLE_SPRITE, boule1->getDimy()*TAILLE_SPRITE);
        }
        if(vag.getTabTireur()[i].getBouleDeFeu2() != NULL && vag.getTabTireur()[i].getBouleDeFeu2()->getEnMouvement())
        {
            im_feu.draw(renderer, boule2->getX()*TAILLE_SPRITE, boule2->getY()*TAILLE_SPRITE, boule2->getDimx()*TAILLE_SPRITE, boule2->getDimy()*TAILLE_SPRITE);
        }
        if(vag.getTabTireur()[i].getBouleDeFeu3() != NULL && vag.getTabTireur()[i].getBouleDeFeu3()->getEnMouvement())
        {
            im_feu.draw(renderer, boule3->getX()*TAILLE_SPRITE, boule3->getY()*TAILLE_SPRITE, boule3->getDimx()*TAILLE_SPRITE, boule3->getDimy()*TAILLE_SPRITE);
        }
    }


    // Afficher le sprite de la flèche
    if (her.getFleche() != NULL && her.getFleche()->getEnMouvement()) {
        string direction = her.getFleche()->getDirectionProjectile();
        int direction2 = 0;
        if (direction == "haut") direction2 = 1;
        if (direction == "bas") direction2 = 2;
        if (direction == "gauche") direction2 = 3;
        if (direction == "droite") direction2 = 4;
        switch (direction2) {

            case 1:
                im_fleche_vers_bas.draw (renderer, fle->getX()*TAILLE_SPRITE, fle->getY()*TAILLE_SPRITE, fle->getDimx()*TAILLE_SPRITE, fle->getDimy()*TAILLE_SPRITE);
				break;

            case 2:
                im_fleche_vers_haut.draw(renderer, fle->getX()*TAILLE_SPRITE, fle->getY()*TAILLE_SPRITE, fle->getDimx()*TAILLE_SPRITE, fle->getDimy()*TAILLE_SPRITE);
				break;

            case 3:
                im_fleche_vers_gauche.draw(renderer, fle->getX()*TAILLE_SPRITE, fle->getY()*TAILLE_SPRITE, fle->getDimx()*TAILLE_SPRITE, fle->getDimy()*TAILLE_SPRITE);
				break;

            case 4:
                im_fleche_vers_droite.draw(renderer, fle->getX()*TAILLE_SPRITE, fle->getY()*TAILLE_SPRITE, fle->getDimx()*TAILLE_SPRITE, fle->getDimy()*TAILLE_SPRITE);
				break;

            default: break;
        }
    }

    // Ecrire un titre par dessus
    /*SDL_Rect positionTitre;
    positionTitre.x = 270;positionTitre.y = 49;positionTitre.w = 100;positionTitre.h = 30;
    SDL_RenderCopy(renderer,font_im.getTexture(),NULL,&positionTitre);*/

    //SDL_RenderDrawLine(renderer, 40, 60, 1540, 60);
    //SDL_RenderDrawLine(renderer, 90, 40, 90, 681);
    SDL_SetRenderDrawColor(renderer, 35, 35, 35, 255);
    /*SDL_Rect rectangle;
    rectangle.x = 40;
    rectangle.y = 40;
    rectangle.w = 1500;
    rectangle.h = 641;
    SDL_RenderDrawRect(renderer, &rectangle);*/

}

void sdlJeu::sdlSon () {

    if (withSound) {

        const Vague& vag = jeu.getConstVague();


        // Jouer les sons des tirs de boules de feu
        for(int i=0; i<3;i++)
        {

            if(vag.getTabTireur()[i].getBouleDeFeu1() != NULL && vag.getTabTireur()[i].getBouleDeFeu1()->getEnMouvement() && vag.getTabTireur()[i].tire)
            {
                Mix_PlayChannel( -1, tirFeu, 0 );
                vag.getTabTireur()[i].tire = false;
            }
            if(vag.getTabTireur()[i].getBouleDeFeu2() != NULL && vag.getTabTireur()[i].getBouleDeFeu2()->getEnMouvement() && vag.getTabTireur()[i].tire)
            {
                Mix_PlayChannel( -1, tirFeu, 0 );
                vag.getTabTireur()[i].tire = false;
            }
            if(vag.getTabTireur()[i].getBouleDeFeu3() != NULL && vag.getTabTireur()[i].getBouleDeFeu3()->getEnMouvement() && vag.getTabTireur()[i].tire)
            {
                Mix_PlayChannel( -1, tirFeu, 0 );
                vag.getTabTireur()[i].tire = false;
            }

        }


        // Jouer les sons de perte de points de vie du héros par les boules de feu
        for(int i=0; i<3;i++)
        {

            if(vag.getTabTireur()[i].getBouleDeFeu1() != NULL && !vag.getTabTireur()[i].getBouleDeFeu1()->getEnMouvement() && vag.getTabTireur()[i].explose)
            {
                Mix_PlayChannel( -1, impactHeros, 0 );
                vag.getTabTireur()[i].explose = false;
            }
            if(vag.getTabTireur()[i].getBouleDeFeu2() != NULL && !vag.getTabTireur()[i].getBouleDeFeu2()->getEnMouvement() && vag.getTabTireur()[i].explose)
            {
                Mix_PlayChannel( -1, impactHeros, 0 );
                vag.getTabTireur()[i].explose = false;
            }
            if(vag.getTabTireur()[i].getBouleDeFeu3() != NULL && !vag.getTabTireur()[i].getBouleDeFeu3()->getEnMouvement() && vag.getTabTireur()[i].explose)
            {
                Mix_PlayChannel( -1, impactHeros, 0 );
                vag.getTabTireur()[i].explose = false;
            }

        }


        // Jouer les sons de perte de points de vie du héros par la massue du troll
        for(int i=0; i<3;i++)
        {

            if((vag.getTabTroll()[i].enVie()) && (vag.getTabTroll()[i].frappe))
            {
                Mix_PlayChannel( -1, impactHeros, 0 );
                vag.getTabTroll()[i].frappe = false;
            }

        }


        // Jouer les sons de perte de points de vie du héros par la massue du gobelin
        for(int i=0; i<3;i++)
        {

            if((vag.getTabGobelin()[i].enVie()) && (vag.getTabGobelin()[i].frappe))
            {
                Mix_PlayChannel( -1, impactHeros, 0 );
                vag.getTabGobelin()[i].frappe = false;
            }

        }


        //Jouer les sons de perte de points de vie des monstres par l'épée du héros
        for(int i=0; i<3; i++)
        {
            //if(vag.getTabGobelin()[i].enVie() || vag.getTabGobelin()[i].vientDeMourir) {
                if(vag.getTabGobelin()[i].seFaitTrancher) {
                    Mix_PlayChannel( -1, impactEpeeSurMonstre, 0 );
                    vag.getTabGobelin()[i].seFaitTrancher = false;
                    vag.getTabGobelin()[i].vientDeMourir = false;
                }
            //}

        }
        for(int i=0; i<3;i++)
        {
            //if(vag.getTabTireur()[i].enVie() || vag.getTabTireur()[i].vientDeMourir) {
                if(vag.getTabTireur()[i].seFaitTrancher) {
                    Mix_PlayChannel( -1, impactEpeeSurMonstre, 0 );
                    vag.getTabTireur()[i].seFaitTrancher = false;
                    vag.getTabTireur()[i].vientDeMourir = false;
                }
            //}
        }
        for(int i=0; i<3;i++)
        {
            //if(vag.getTabTroll()[i].enVie() || vag.getTabTroll()[i].vientDeMourir) {
                if(vag.getTabTroll()[i].seFaitTrancher) {
                    Mix_PlayChannel( -1, impactEpeeSurMonstre, 0 );
                    vag.getTabTroll()[i].seFaitTrancher = false;
                    vag.getTabTroll()[i].vientDeMourir = false;
                }
            //}
        }


        //Jouer les sons de perte de points de vie des monstres par une flèche du héros
        for(int i=0; i<3; i++)
        {
            //if(vag.getTabGobelin()[i].enVie() || vag.getTabGobelin()[i].vientDeMourir) {
                if(vag.getTabGobelin()[i].seFaitTranspercer) {
                    Mix_PlayChannel( -1, impactFlecheSurMonstre, 0 );
                    vag.getTabGobelin()[i].vientDeMourir = false;
                    vag.getTabGobelin()[i].seFaitTranspercer = false;
                }
            //}

        }
        for(int i=0; i<3;i++)
        {
            //if(vag.getTabTireur()[i].enVie() || vag.getTabTireur()[i].vientDeMourir) {
                if(vag.getTabTireur()[i].seFaitTranspercer) {
                    Mix_PlayChannel( -1, impactFlecheSurMonstre, 0 );
                    vag.getTabTireur()[i].vientDeMourir = false;
                    vag.getTabTireur()[i].seFaitTranspercer = false;
                }
            //}
        }
        for(int i=0; i<3;i++)
        {
            //if(vag.getTabTroll()[i].enVie() || vag.getTabTroll()[i].vientDeMourir) {
                if(vag.getTabTroll()[i].seFaitTranspercer) {
                    Mix_PlayChannel( -1, impactFlecheSurMonstre, 0 );
                    vag.getTabTroll()[i].vientDeMourir = false;
                    vag.getTabTroll()[i].seFaitTranspercer = false;
                }
            //}
        }

    }

}

bool sdlJeu::sdlMenu(int menu)
{

        SDL_Event evt;
        int dimx, dimy;
        dimx = jeu.getConstTerrain().getDimX();
        dimy = jeu.getConstTerrain().getDimY();
        dimx = dimx * TAILLE_SPRITE + 28;
        dimy = dimy * TAILLE_SPRITE + 17;
        /*SDL_Rect positionTitre;
        positionTitre.x = 250;
        positionTitre.y = 50;
        positionTitre.w = 100;
        positionTitre.h = 30;*/
        SDL_Rect bouton1;
        bouton1.x = 620;
        bouton1.w = 330;
        bouton1.y = 400;
        bouton1.h = 64;
        SDL_Rect bouton2;
        bouton2.x = 620;
        bouton2.w = 330;
        bouton2.y = 536;
        bouton2.h = 64;
        /*SDL_Rect titreJeu;
        titreJeu.x = 450;
        titreJeu.w = 600;
        titreJeu.y = 160;
        titreJeu.h = 150;*/
        bool continuer=true;
        while(menu != 0)
        {
            if(menu == 1) /// 1 = menu principal
            {
                SDL_SetRenderDrawColor(renderer, 105, 100, 89, 255);
                SDL_RenderClear(renderer);
                im_menu.draw(renderer,0,0,dimx,dimy);/// changer im_menu en im_menuPrincipal
               // SDL_RenderCopy(renderer, font_menu.getTexture(), NULL, &positionTitre);
                SDL_RenderCopy(renderer, font_jouer.getTexture(), NULL, &bouton1);
                SDL_RenderCopy(renderer, font_quitter.getTexture(), NULL, &bouton2);
                //SDL_RenderCopy(renderer, font_titre_du_jeu.getTexture(), NULL, &titreJeu);
                SDL_RenderPresent(renderer);
                continuer = true;

                /// AJOUTE 2 boutons textes avec Jouer et Quitter
            }

            else if(menu == 2) ///2 = menu pause
            {
                SDL_SetRenderDrawColor(renderer, 105, 100, 89, 255);
                SDL_RenderClear(renderer);
                im_menu.draw(renderer,0,0,dimx,dimy); /// changer im_menu en im_menuPause
                //SDL_RenderCopy(renderer, font_pause.getTexture(), NULL, &positionTitre);
                SDL_RenderCopy(renderer, font_reprendre.getTexture(), NULL, &bouton1);
                SDL_RenderCopy(renderer, font_retourMenu.getTexture(), NULL, &bouton2);
                SDL_RenderPresent(renderer);
                continuer = true;

            }
            while(continuer)
            {
                while(SDL_PollEvent(&evt))
                {
                    switch(evt.type)
                    {
                        case SDL_QUIT:
                        continuer = false;
                        return true;
                        break;

                        case SDL_KEYDOWN:

                            switch(evt.key.keysym.sym)
                            {
                                case SDLK_q:
                                continuer = false;
                                return true;
                                break;

                                case SDLK_ESCAPE:
                                continuer=false;
                                return true;
                                break;
                            }

                        case SDL_MOUSEBUTTONUP:
                            if(evt.button.button==SDL_BUTTON_LEFT)
                            {
                                if(evt.button.x>512 && evt.button.x<1042 && evt.button.y>400 && evt.button.y<475) ///Appuie sur le bouton "Jouer une partie"
                                {
                                    if(menu == 1)
                                    {
                                        continuer=false;
                                        menu = 0;
                                        return false;
                                    }
                                    if(menu ==2)
                                    {
                                        continuer = false;
                                        menu = 0;
                                        return false;
                                    }
                                }
                                if(evt.button.x>512 && evt.button.x<1042 && evt.button.y>516 && evt.button.y<600) ///Appuie sur le bouton quitter
                                {
                                    if(menu == 1)
                                    {
                                        continuer=false;
                                        menu = 0;
                                        return true;
                                    }
                                    if(menu==2)
                                    {
                                        continuer = false;
                                        menu = 1;
                                        this->jeu = Jeu(1);
                                    }
                                }
                            }
                        }
                    }
                SDL_Delay(20);
                continuer=false;
            }
        }
    SDL_Quit();
    return false;
}

void sdlJeu::sdlBoucle () {

    float tempsDernierTraitementAttaqueEpee = 0;
    float tempsDernierTraitementAttaqueArc = 0;

    const Heros& herPourFlePourSon = jeu.getConstHeros();

    // on lance la musique, qui est automatiquement relancée du début quand elle arrive à son terme, à l'infini (tant qu'on n'a pas quitté le programme)
    Mix_PlayMusic(sound, -1);

    SDL_Event events;
	bool quit = false;
	quit = sdlMenu(1);

    //Uint32 t = SDL_GetTicks(), nt;

	// tant que ce n'est pas la fin ...

    while(!quit)
    {

        usleep(100000);

        // !!! CREUSER CES CINQ LIGNES POUR REDUIRE LAGUE ? POUR GERER FREQUENCE D'IMAGES...
        //nt = SDL_GetTicks();
        //if (nt-t>500) {
            jeu.actionsAutomatiques();
            //t = nt;
        //}

        //Joue les sons, s'il y a des sons à jouer et si withSound est vrai
        sdlSon ();

		// tant qu'il y a des evenements à traiter (cette boucle n'est pas bloquante)
		while (SDL_PollEvent(&events)) {
			if (events.type == SDL_QUIT)
			{quit = true;}           // Si l'utilisateur a clique sur la croix de fermeture
			else if (events.type == SDL_KEYDOWN) {              // Si une touche est enfoncee
				switch (events.key.keysym.scancode) {

				case SDL_SCANCODE_E:
					jeu.actionClavier('b');    // car Y inverse
					break;

				case SDL_SCANCODE_D:
					jeu.actionClavier('h');     // car Y inverse
					break;

				case SDL_SCANCODE_S:
					jeu.actionClavier('g');
					break;

				case SDL_SCANCODE_F:
					jeu.actionClavier('d');
					break;

                case SDL_SCANCODE_SPACE: //touche "espace"
                {
                    clock_t tCEpee = clock();
	                float tCEpeeSec = ((float)tCEpee/CLOCKS_PER_SEC);

                    if (tCEpeeSec - tempsDernierTraitementAttaqueEpee > 0.01) {
                        if(withSound)
                            Mix_PlayChannel( -1, coupEpee, 0 );
				        jeu.actionClavier('e'); //'e' comme "épée"
                        tempsDernierTraitementAttaqueEpee = tCEpeeSec;
                    }
                }
				    break;

                case SDL_SCANCODE_V:
                {
                    if (herPourFlePourSon.getFleche() == NULL || !(herPourFlePourSon.getFleche()->getEnMouvement())) {

                        clock_t tCArc = clock();
                        float tCArcSec = ((float)tCArc/CLOCKS_PER_SEC);

                        if (tCArcSec - tempsDernierTraitementAttaqueArc > 0.02) {
                            if(withSound)
                                Mix_PlayChannel( -1, tirArc, 0 );
                            jeu.actionClavier('a'); //'a' comme "arc"
                            tempsDernierTraitementAttaqueArc = tCArcSec;
                        }
                    }
                }
				    break;

			    case SDL_SCANCODE_Y: //touche 'y'
				    jeu.actionClavier('s'); //'s' comme "super-état"
				    break;

			    case SDL_SCANCODE_P: //touche 'p'
				    quit=sdlMenu(2);
				    break;

			    case SDL_SCANCODE_U: //touche 'u'
				    jeu.actionClavier('t'); //'t' comme "tue-les tous"
				    break;

                case SDL_SCANCODE_ESCAPE:
                case SDL_SCANCODE_Q:
                    quit = true;
                    break;
                    
				default: break;
				}
				/*if ((withSound) && (mangePastille))
                    Mix_PlayChannel(-1,sound,0);*/
                //Il y aura ici plein de lignes sur le format des 2 lignes ci-dessus (mais il n'y aura pas les 2 lignes ci-dessus), avec une condition (une variable ?) à la place de mangePastille
			}
		}

		// on affiche le jeu sur le buffer caché
		sdlAff();

		// on permute les deux buffers (cette fonction ne doit se faire qu'une seule fois dans la boucle)
        SDL_RenderPresent(renderer);
	}
}

