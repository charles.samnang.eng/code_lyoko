#ifndef _SDLJEU_H
#define _SDLJEU_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include "../core/Jeu.h"

//! \brief Pour gérer une image avec SDL2
class Image {

private:

    SDL_Surface * surface;
    SDL_Texture * texture;
    bool has_changed;

public:
    Image () ;
    void loadFromFile (const char* filename, SDL_Renderer * renderer);
    void loadFromCurrentSurface (SDL_Renderer * renderer);
    void draw (SDL_Renderer * renderer, int x, int y, int w=-1, int h=-1);
    SDL_Texture * getTexture() const;
    void setSurface(SDL_Surface * surf);
};



/**
    La classe gérant le jeu avec un affichage SDL
*/
class sdlJeu {

private :

	Jeu jeu;

    SDL_Window * window;
    SDL_Renderer * renderer;

    TTF_Font * font;
    TTF_Font * font_2;
    Image font_im;
    Image font_jouer;
    Image font_quitter;
    Image font_retourMenu;
    Image font_reprendre;
    Image font_titre_du_jeu;
    SDL_Color font_color;
    SDL_Color font_color_2;

    Mix_Music * sound;
    Mix_Chunk * coupEpee;
    Mix_Chunk * tirArc;
    Mix_Chunk * tirFeu;
    Mix_Chunk * impactFeu;
    Mix_Chunk * impactEpeeSurMonstre;
    Mix_Chunk * impactFlecheSurMonstre;
    Mix_Chunk * impactHeros;
    Mix_Chunk * coupMassueTroll;
    Mix_Chunk * coupMassueGobelin;
    bool withSound;

    Image im_heros_vers_haut;
    Image im_heros_vers_bas;
    Image im_heros_vers_gauche;
    Image im_heros_vers_droite;
    Image im_mur;
    Image im_sol;
    Image im_sol2;
    Image im_fleche_vers_haut;
    Image im_fleche_vers_bas;;
    Image im_fleche_vers_gauche;
    Image im_fleche_vers_droite;
    Image im_troll_vers_haut;
    Image im_troll_vers_bas;
    Image im_troll_vers_gauche;
    Image im_troll_vers_droite;
    Image im_gobelin_vers_gauche;
    Image im_gobelin_vers_droite;
    Image im_tireur_vers_gauche;
    Image im_tireur_vers_droite;
    Image im_feu;
    Image im_epee;
    Image im_massue;
    Image im_petite_massue;
    Image im_menu;
    Image im_map;

    bool souris;
    bool touche;

public :

    sdlJeu ();
    ~sdlJeu ();
    bool sdlMenu(int menu);
    void sdlBoucle ();
    void sdlAff ();
    void sdlSon ();

};

#endif
