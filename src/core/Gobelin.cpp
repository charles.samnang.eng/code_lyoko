#include "Gobelin.h"
#include <cmath>

Gobelin::Gobelin()
{}

Gobelin::Gobelin (unsigned int posX, unsigned int posY) {

// Données membres de Personnage
    pdv = 0;
    x = posX;
    y = posY;
    directionVisee = "droite"; // "bas" remplacé par "droite" récemment
    directionViseeX = 0;
    directionViseeY = 0;
    hauteur = 8;
    largeur = 8;
    hitboxHauteur = 8;
    hitboxLargeur = 8;
    vientDeMourir = false;


// Données membres de Monstre
    dir = 0;
    enMouvement = false;
    attaque = false;
    degatsAttaqueBasique = 3;
    porteAttaqueBasique = 10;
    vitesseAttaque = 10; /// pas encore sûr, à définir
    vitesseDeplacement = 3; /// pas encore sûr, à définir
    tempsEnClicks = 0;
    tempsEnSec = 0;
    intouchable = false;
    seFaitTrancher = false;
    seFaitTranspercer = false;

    

// Données membres de Gobelin
    tempsDernierTraitement = 0;
    tempsDernierTraitement2 = 0;
    attaque = false;
    tempsDebutAttaque = 0;
    frappe = false;

}


void Gobelin::versHeros (const Terrain & t, const Heros & h) {

    clock_t te = clock();
	float teS = ((float)te/CLOCKS_PER_SEC);

    if (teS - tempsDernierTraitement > vitesseDeplacement/1000) {

        int dx = h.getX()-x;
        int dy = h.getY()-y;
        std::string direction = "droite";
        if (dx>0) {
            dx=1;
            direction = "droite";
        }
        if (dx<0) {
            dx=-1;
            direction = "gauche";
        }
        if (dy>0) dy=1;
        if (dy<0) dy=-1;

        if (

            /* Chantier en cours, pas encore eu le temps de finir (Raph). A la place des 1 et 2, c'est une donnée membre sur la portée de l'attaque du
            Gobelin qui sera utilisée. En effet, un monstre qui peut attaquer à 2 cases de distance au lieu d'1 ne cherchera pas à s'avancer à une case du
            héros.*/

            t.estPositionPersoValide(x+dx,y+dy)
            && !(h.getX()==getX() && (h.getY()==getY()-1 /*|| h.getY()==getY()-2*/))
            && !(h.getX()==getX() && (h.getY()==getY()+1 /*|| h.getY()==getY()+2*/))
            && !(h.getY()==getY() && (h.getX()==getX()-1 /*|| h.getX()==getX()-2*/))
            && !(h.getY()==getY() && (h.getX()==getX()+1 /*|| h.getX()==getX()+2*/))

            && !(h.getX()==getX()+1 && h.getY()==getY()+1)
            && !(h.getX()==getX()+1 && h.getY()==getY()-1)
            && !(h.getX()==getX()-1 && h.getY()==getY()+1)
            && !(h.getX()==getX()-1 && h.getY()==getY()-1)
            ) {
            x = x+dx;
            y = y+dy;
            if (direction == "droite") directionVisee = "droite";
            else directionVisee = "gauche";
        }

        tempsDernierTraitement = teS;

    }

}


void Gobelin::coupDePetiteMassue (Terrain & t, const Heros & h) {

    if (pdv > 0) {

        float absHeros = ((float) h.getX());
        float ordHeros = ((float) h.getY());
        float absGobelin = ((float) x);
        float ordGobelin = ((float) y);

        float distanceGobelinHeros = sqrt(pow(absHeros-absGobelin,2)+pow(ordHeros-ordGobelin,2));

        float portee = ((float) porteAttaqueBasique);

        clock_t te2 = clock();
        float teS2 = ((float)te2/CLOCKS_PER_SEC)*100;

        if (teS2 - tempsDernierTraitement2 > vitesseAttaque) {

            if(x <= h.getX() && y > h.getY()) {

                if (distanceGobelinHeros - hitboxLargeur <= portee) {

                    for(unsigned int j=y-porteAttaqueBasique; j<y; j++)
                    {
                        for(unsigned int i=x; i<=x+porteAttaqueBasique; i++) {
                            t.setXY(i,j,'k');
                        }

                    }
                    attaque=true;
                    clock_t t = clock();
                    tempsDebutAttaque = float(t);

                }

            }


            if(x < h.getX() && y <= h.getY()) {

                if (distanceGobelinHeros - hitboxLargeur <= portee) {

                    for(unsigned int j=y; j<=y+porteAttaqueBasique; j++)
                    {
                        for(unsigned int i=x+1; i<=x+porteAttaqueBasique; i++) {
                            t.setXY(i,j,'k');
                        }

                    }
                    attaque=true;
                    clock_t t = clock();
                    tempsDebutAttaque = float(t);

                }

            }


            if(x >= h.getX() && y < h.getY()) {

                if (distanceGobelinHeros - hitboxLargeur <= portee) {

                    for(unsigned int j=y+1; j<=y+porteAttaqueBasique; j++)
                    {
                        for(unsigned int i=x-porteAttaqueBasique; i<=x; i++) {
                            t.setXY(i,j,'k');
                        }

                    }
                    attaque=true;
                    clock_t t = clock();
                    tempsDebutAttaque = float(t);

                }

            }


            if(x > h.getX() && y >= h.getY()) {

                if (distanceGobelinHeros - hitboxLargeur <= portee) {

                    for(unsigned int j=y-porteAttaqueBasique; j<=y; j++)
                    {
                        for(unsigned int i=x-porteAttaqueBasique; i<x; i++) {
                            t.setXY(i,j,'k');
                        }

                    }
                    attaque=true;
                    clock_t t = clock();
                    tempsDebutAttaque = float(t);

                }

            }


            tempsDernierTraitement2 = teS2;
            

        }

    }

}


void Gobelin::tempsAttaque(Terrain & t, Heros & h)
{
    if(getAtk())
    {
        clock_t ta = clock();
        if(ta-tempsDebutAttaque>5000)
        {
            setAtk(false);
            for(int i=0; i<t.getDimX();i++)
            {
                for(int j=0; j<t.getDimY();j++)
                {
                    if(t.getXY(i,j)=='k') t.setXY(i,j,' ');
                }
            }
        }
    }
}


bool Gobelin::getAtk(){return attaque;}
void Gobelin::setAtk(bool b){attaque=b;}


void Gobelin::actionAuto2(Terrain & t, Heros & h)
{
    tempsAttaque(t,h);
}