/**
@brief Module gérant un gobelin
@file Gobelin.h
*/

#ifndef _GOBELIN_H
#define _GOBELIN_H

#include "Monstre.h"

/**
@brief Un gobelin est un des types de monstres
*/
class Gobelin : public Monstre
{
    private :

        float tempsDernierTraitement;
        float tempsDernierTraitement2;
        bool attaque;
        float tempsDebutAttaque;
        

    public :

        Gobelin();
        Gobelin(unsigned int posX, unsigned int posY);
        void versHeros (const Terrain & t, const Heros & h);
        void coupDePetiteMassue (Terrain & t, const Heros & h);
        void tempsAttaque(Terrain & t, Heros & h);
        bool getAtk();
        void setAtk(bool b);
        void actionAuto2(Terrain & t, Heros & h);

};

#endif // _GOBELIN_H
