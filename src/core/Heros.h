#ifndef _HEROS_H
#define _HEROS_H

#include "Terrain.h"
#include "Personnage.h"
#include "Fleche.h"
#include "time.h"


class Monstre;
class Tireur;
class Heros : public Personnage
{
    private:
        unsigned int jaugeTransformation;
        bool estTransforme;
        int degatsAttaqueEpee;
        double vitesseAttaque;
        bool estTouche;
        bool attaque;
        bool invincible;
        Fleche* fleche;
        float tempsDebutInvincibilite;
        float tempsDebutAttaque;
       // time_t debutAtk; // !!! Attention : time_t contient une valeur en secondes dépendant de l'OS (donc il y a une opération à faire pour avoir une valeur indépendante de l'OS). // Utiliser plutôt clock_t (CM5 LIFAP4) partout ?

    public:

        Heros();
        ~Heros();

        void attaqueEpee(Terrain & t);
        void attaqueEpee2(Terrain & t);
        void attaqueArc(Terrain & t);
        void seTransforme();
        void annuleTransforme();
        bool collisionMonstre(const Monstre & m);
        void recevoirDegats(const Terrain & t, Monstre & m);
        void recevoirDegatsProjectile(Tireur & tireur);
        bool collisionProjectile(const Tireur & tireur);
        bool collisionProjectile2(const Tireur & tireur);
        bool collisionProjectile3(const Tireur & tireur);
        void tempsInvincible();
        void tempsAttaque(Terrain & t);

        void gauche (const Terrain & t);
        void droite (const Terrain & t);
        void haut (const Terrain & t);
        void bas (const Terrain & t);

        void actionAuto(Terrain & t, Monstre & m);
        void actionAuto(Terrain & t, Tireur & tireur);
        void actionAuto(Terrain & t);

        void setAtk(bool b);
        bool getAtk();
        void setX(int posX);
        void setY(int posY);
        void setInvincible(bool b);
        bool getInvincible();
        int getDegatsAttaqueEpee () const;
        std::string getDirectionVisee() const;
        Fleche* getFleche() const;
        void detruireFleche();
        bool getTransforme() const;
        unsigned int getJaugeTransformation() const;
        void setJaugeTransformation(unsigned int i);
};

#endif // _HEROS_H
