#include "BouleDeFeu.h"

BouleDeFeu::BouleDeFeu(unsigned int posX, unsigned int posY, std::string dir){
    x = posX;
    y = posY;
    directionProjectile = dir;
    dimx = 5;
    dimy = 5;
    hitboxX = 3;
    hitboxY = 3;
    enMouvement = true;
    tempsDernierDeplacement = 0;
    vitesseProjectile = 1;
    degatsProjectile = 25;
}
