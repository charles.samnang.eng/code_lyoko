#include "Projectile.h"

void Projectile::gauche (const Terrain & t) {
    clock_t te=clock();
    float teS = float(te)/CLOCKS_PER_SEC;
    if(teS-tempsDernierDeplacement > vitesseProjectile/1000)
    {
        if (t.estPositionPersoValide(x-1,y)) x=x-2;
        else enMouvement=false;
        tempsDernierDeplacement = teS;
    }
}

void Projectile::droite (const Terrain & t) {
    clock_t te=clock();
    float teS = float(te)/CLOCKS_PER_SEC;
    if(teS-tempsDernierDeplacement > vitesseProjectile/1000)
    {
        if (t.estPositionPersoValide(x+dimx,y)) x=x+2;
        else enMouvement=false;
        tempsDernierDeplacement = teS;
    }
}

void Projectile::haut (const Terrain & t) {
    clock_t te=clock();
    float teS = float(te)/CLOCKS_PER_SEC;
    if(teS-tempsDernierDeplacement > vitesseProjectile/1000)
	{
        if (t.estPositionPersoValide(x,y+dimy)) y=y+2;
        else enMouvement=false;
        tempsDernierDeplacement = teS;
    }
}

void Projectile::bas (const Terrain & t) {

    clock_t te=clock();
    float teS = float(te)/CLOCKS_PER_SEC;
    if(teS-tempsDernierDeplacement > vitesseProjectile/1000)
    {
        if (t.estPositionPersoValide(x,y-2)) y=y-2;
        else enMouvement=false;
        tempsDernierDeplacement = teS;
    }
}

unsigned int Projectile::getDegatsProjectile() const {return degatsProjectile;}
std::string Projectile::getDirectionProjectile() const{ return directionProjectile;}
unsigned int Projectile::getX() const {return x;}
unsigned int Projectile::getY() const {return y;}
unsigned int Projectile::getDimx() const {return dimx;}
unsigned int Projectile::getDimy() const {return dimy;}
unsigned int Projectile::getHitboxX() const {return hitboxX;}
unsigned int Projectile::getHitboxY() const {return hitboxY;}
bool Projectile::getEnMouvement() const { return enMouvement;}
void Projectile::setEnMouvement(bool b) {enMouvement = b;}
