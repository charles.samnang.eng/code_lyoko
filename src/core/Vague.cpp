#include "Vague.h"

Vague:: Vague(int lvl) {

    nbTroll = 0;
    nbGobelin = 0;
    nbTireur = 0;
    tabTroll = new Troll[3];
    tabTroll[0] = Troll(40,10);
    tabTroll[1] = Troll(80,10);
    tabTroll[2] = Troll(120,10);
    tabGobelin = new Gobelin[3];
    tabGobelin[0] = Gobelin(120,10);
    tabGobelin[1] = Gobelin(80,10);
    tabGobelin[2] = Gobelin(40,10);
    tabTireur = new Tireur[3];
    tabTireur[0] = Tireur(10,10);
    tabTireur[1] = Tireur(61,10);
    tabTireur[2] = Tireur(120,10);
    switch(lvl)
    {
        case 0:
        nbTireur = 2;
        tabTireur[0].setPdv(20);
        tabTireur[1].setPdv(20);
        nbGobelin=1;
        tabGobelin[0].setPdv(30);
        /*nbTroll = 1;
        tabTroll[0].setPdv(50);*/
        break;

        case 1:
        nbTireur = 2;
        nbTroll = 1;
        nbGobelin = 2;
        tabGobelin[0].setPdv(30);
        tabGobelin[1].setPdv(30);
        tabTireur[0].setPdv(20);
        tabTireur[1].setPdv(20);
        tabTroll[0].setPdv(50);
        break;

        case 2:
        nbTireur = 2;
        nbGobelin = 1;
        nbTroll = 2;
        tabTroll[0].setPdv(70);
        tabTroll[1].setPdv(70);
        tabGobelin[0].setPdv(40);
        tabTireur[0].setPdv(20);
        tabTireur[1].setPdv(20);
        break;

        case 3:
        nbGobelin = 2;
        tabGobelin[0].setPdv(50);
        tabGobelin[1].setPdv(50);
        nbTroll = 3;
        tabTroll[0].setPdv(80);
        tabTroll[1].setPdv(80);
        tabTroll[2].setPdv(80);
        break;

        case 4:
        nbTroll = 3;
        nbGobelin = 3;
        nbTireur = 3;
        tabTroll[0].setPdv(100);
        tabTroll[1].setPdv(100);
        tabTroll[2].setPdv(100);
        tabGobelin[0].setPdv(50);
        tabGobelin[1].setPdv(50);
        tabGobelin[2].setPdv(50);
        tabTireur[0].setPdv(20);
        tabTireur[1].setPdv(20);
        tabTireur[2].setPdv(20);
        break;

        default:
        break;

    }
    nbMonstres = nbGobelin+nbTroll+nbTireur;
}


int Vague::getNbMonstres() const
{
    return nbMonstres;
}

Troll* Vague::getTabTroll() const
{
    return tabTroll;
}

Gobelin* Vague::getTabGobelin() const
{
    return tabGobelin;
}

Tireur* Vague::getTabTireur() const
{
    return tabTireur;
}

int Vague::getNbTroll() const
{
    return nbTroll;
}

int Vague::getNbGobelin() const
{
    return nbGobelin;
}

int Vague::getNbTireur() const
{
    return nbTireur;
}

Vague::~Vague()
{

}

int Vague::getNbMonstresVivant() const
{
    int nbMonstresVivant = nbMonstres;
    for(int i=0; i<nbTroll; i++)
    {
        if(!tabTroll[i].enVie() && nbTroll>0)
        {
            nbMonstresVivant--;
        }
    }
    for(int i=0; i<nbGobelin; i++)
    {
        if(!tabGobelin[i].enVie() && nbGobelin>0)
        {
            nbMonstresVivant--;
        }
    }
    for(int i=0; i<nbTireur; i++)
    {
        if(!tabTireur[i].enVie() && nbTireur>0)
        {
            nbMonstresVivant--;
        }
    }
    return nbMonstresVivant;
}
