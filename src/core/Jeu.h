/**
@brief Module gérant un Jeu

@file Jeu.h
@date 2021/03/12
*/

#ifndef _JEU_H
#define _JEU_H

#include "Personnage.h"
#include "Monstre.h"
#include "Heros.h"
#include "Terrain.h"
#include "Fleche.h"
#include "Troll.h"
#include "Gobelin.h"
/*#include "Sanglier.h"
#include "Scarabee.h"
#include "Tireur.h"
#include "Pierre.h"
#include "Feu.h"*/

#include "Vague.h"

#include <ctime>

/**
@brief Un jeu = un terrain, un héros, des monstres (trolls, gobelins, sangliers, scarabées et tireurs) et des projectiles (flèches, pierres et feu)
*/
class Jeu {

private :

    int lvl;
	Terrain ter;
	Heros her;
	Vague vag;
	//Troll tro;
    //Gobelin gob;
    //Laisser ces 3 données membres ? Faire peut-être un tableau de 50 projectiles par type de projectile
    /*Fleche fle;
    Pierre pie;
    Feu feu;*/

    int variableTest;

public :

    Jeu (int lvl);
    ~Jeu ();


    //GETTERS PEUT-ÊTRE INCORRECTS (notamment au niveau du type de ce qui est renvoyé) et peut-être certains inutiles

    //Remplacer les gets pas getConst par des setters ?
    Terrain& getTerrain ();
    Heros& getHeros ();
    Vague& getVague();
    //Troll& getTroll ();
   // Gobelin& getGobelin ();
    /*Sanglier& getSanglier ();
    Scarabee& getScarabee ();
    Tireur& getTireur ();

    Pierre& getPierre ();
    Feu& getFeu ();*/



    const Terrain& getConstTerrain () const;
    const Heros& getConstHeros () const;
    const Vague& getConstVague () const;
    //const Troll& getConstTroll () const;
    //const Gobelin& getConstGobelin () const;
    /*const Sanglier& getConstSanglier () const;
    const Scarabee& getConstScarabee () const;
    const Tireur& getConstTireur () const;

    const Pierre& getConstPierre () const;
    const Feu& getConstFeu () const;*/




    Heros getHeros () const;

    int getNombreDeFantome() const; // à modifier ou supprimer selon si la base peut être utile ou non

    void actionsAutomatiques ();
    void actionClavier(const char touche);
    //void pause (); //la procédure pause doit avoir un (ou plusieurs) paramètre(s) ? Il ne doit pas y avoir de break ? Chercher comment implémenter la fonctionnalité pause

};

#endif
