/**
@brief Module gérant un troll
@file Troll.h
@date 2021/03/21
*/

#ifndef _TROLL_H
#define _TROLL_H

#include "Monstre.h"
//#include "Pierre.h"

/**
@brief Un troll est un des types de monstres
*/
class Troll : public Monstre {

    private:

        float tempsDernierTraitement;
        float tempsDernierTraitement2;
        bool attaque;
        float tempsDebutAttaque;


    public:

        int variableTest;

        Troll();
        Troll(int x, int y);

        void versHeros (const Terrain & t, const Heros & h); // IA. Raph doit la coder

        void coupDeMassue (Terrain & t, const Heros & h);
        //void lancerDePierre (const Pierre & pie) const;

        void tempsAttaque(Terrain & t, Heros & h);

        bool getAtk();
        void setAtk(bool b);
        void actionAuto2(Terrain & t, Heros & h);

};

#endif
