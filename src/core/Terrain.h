/**
@brief Module gérant un terrain dans l'application

Un terrain dans l'application est un tableau 2D de caractères dont la taille est dimx x dimy.
Un terrain ne peut pas dépasser 300x300 cases (ATTENTION : il n'y pas de vérification de ceci).

@file Terrain.h
@date 2021/03/12
*/

#ifndef _TERRAIN_H
#define _TERRAIN_H

/**
@brief La classe Terrain contient ses dimensions et un tab 2D de cases (une case=1 char)
*/
class Terrain {

private :

	int dimx;
	int dimy;
	char ter [1080][1920]; // par la suite peut-être char ter [40][156] à la place

public :

    /**
    @brief <B>Construit</B> un objet Terrain.
    Le constructeur remplit dimx et dimy et les cases du tableau ter avec un terrain par défaut.
    @bug S'il y a un bug dans le constructeur, je peux le signaler ici ...
    */
    Terrain();

    /**
    @brief Détruit un objet Terrain.
    Le destructeur met des 0 dans toutes les cases du tableau ter puis donne à dimx et dimy la valeur -1.
    @bug S'il y a un bug dans le destructeur, je peux le signaler ici ...
    */
    ~Terrain();

    /**
    @brief Renvoie vrai si on peut positionnner un personnage aux coordonnées (x,y), faux sinon
    @param x : abs de la case à tester
    @param y : ordonnée de la case à tester
    */
    bool estPositionPersoValide (const int x, const int y) const;

    /**
    @brief Renvoie le type d'objet se trouvant en (x,y)
    @param x : abs de la case de l'objet
    @param y : ordonnée de la case de l'objet
    */
    char getXY (const int x, const int y) const;

    /**
    @brief Renvoie la largeur du terrain
    */
    int getDimX () const;

    /**
    @brief Renvoie la hauteur du terrain
    */
    int getDimY () const;



    /**
    @brief Place un objet en (x,y)
    @param x : abs de la case du nouvel objet
    @param y : ordonnée de la case du nouvel objet
    @param objet : nouvel objet
    */
    void setXY (const int x, const int y, char objet);

    //procédure peut-être pas nécessaire
    /**
    @brief Change la largeur du terrain
    @param x : nouvelle largeur du terrain
    */
    void setDimX (const int & x);

    //procédure peut-être pas nécessaire
    /**
    @brief Change la hauteur du terrain
    @param y : nouvelle hauteur du terrain
    */
    void setDimY (const int & y);
    void effacerTerrain(unsigned int x, unsigned int y);

    void testRegression ();
};

#endif
