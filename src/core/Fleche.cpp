#include "Fleche.h"

Fleche::Fleche(unsigned int posX, unsigned int posY, std::string dir, bool etatTransformation){
    x = posX;
    y = posY;
    directionProjectile = dir;
    if(dir =="haut" || dir == "bas")
    {
        dimx = 4;
        dimy = 10;
        hitboxX = 3;
        hitboxY = 8;
    }
    else { dimx = 10; dimy = 4; hitboxX = 8; hitboxY = 3;}
    enMouvement = true;
    tempsDernierDeplacement = 0;
    vitesseProjectile = 1;
    if(!etatTransformation)
    {
        degatsProjectile = 10;
    }
    else degatsProjectile = 20;
}

void Fleche::gauche (const Terrain & t) {
    clock_t te=clock();
    float teS = float(te)/CLOCKS_PER_SEC;
    if(teS-tempsDernierDeplacement > vitesseProjectile/1000)
    {
        if (t.estPositionPersoValide(x-3,y)) x=x-3;
        else enMouvement=false;
        tempsDernierDeplacement = teS;
    }
}

void Fleche::droite (const Terrain & t) {
    clock_t te=clock();
    float teS = float(te)/CLOCKS_PER_SEC;
    if(teS-tempsDernierDeplacement > vitesseProjectile/1000)
    {
        if (t.estPositionPersoValide(x+3,y)) x=x+3;
        else enMouvement=false;
        tempsDernierDeplacement = teS;
    }
}

void Fleche::haut (const Terrain & t) {
    clock_t te=clock();
    float teS = float(te)/CLOCKS_PER_SEC;
    if(teS-tempsDernierDeplacement > vitesseProjectile/1000)
	{
        if (t.estPositionPersoValide(x,y+3)) y=y+3;
        else enMouvement=false;
        tempsDernierDeplacement = teS;
    }
}

void Fleche::bas (const Terrain & t) {

    clock_t te=clock();
    float teS = float(te)/CLOCKS_PER_SEC;
    if(teS-tempsDernierDeplacement > vitesseProjectile/1000)
    {
        if (t.estPositionPersoValide(x,y-3)) y=y-3;
        else enMouvement=false;
        tempsDernierDeplacement = teS;
    }
}