#ifndef TIREUR_H_INCLUDED
#define TIREUR_H_INCLUDED

#include "Monstre.h"
#include "BouleDeFeu.h"

/**
@brief Un tireur est un des types de monstres
*/
class Tireur : public Monstre
{
    private :

        float tempsDernierTir; // Cette donnée membre aura un autre nom
        BouleDeFeu* fireball1;
        BouleDeFeu* fireball2;
        BouleDeFeu* fireball3;

    public :

        bool tire, explose;

        Tireur();
        Tireur(unsigned int posX, unsigned int posY);
        void bougeAuto(const Terrain & t);
        bool herosAPortee(const Heros & h);
        void tirerProjectile(const Heros & h);
        void deplacementProjectile(const Terrain & t);
        BouleDeFeu* getBouleDeFeu1() const;
        BouleDeFeu* getBouleDeFeu2() const;
        BouleDeFeu* getBouleDeFeu3() const;
};


#endif // TIREUR_H_INCLUDED
