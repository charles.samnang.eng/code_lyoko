#ifndef BOULEDEFEU_H_INCLUDED
#define BOULEDEFEU_H_INCLUDED

#include "Projectile.h"

class BouleDeFeu : public Projectile
{
    public :
    BouleDeFeu(unsigned int posX, unsigned int posY, std::string dir);
};

#endif // BOULEDEFEU_H_INCLUDED
