#include "Heros.h"
#include "Monstre.h"
#include "Tireur.h"

const int AJUSTEMENT_VISUEL_FLECHE = 3;

///Beaucoup de paramètres à tester/définir tel que la position initiale, la largeur/hauteur, degats,vitesse attaque & déplacement. Pour l'instant mis des valeurs "plausibles" pour test.
Heros::Heros(){

// données membres de Personnage.
    pdv = 100;
    x = 77;
    y = 55;
    directionVisee = "droite";
    directionViseeX = 0;
    directionViseeY = 0;
    hauteur = 10;
    largeur = 10;
    hitboxHauteur = 8;
    hitboxLargeur = 8;
    vientDeMourir = false;

//données membres de Héros
    jaugeTransformation = 0;
    degatsAttaqueEpee = 20;
    vitesseAttaque = 1;
    invincible = false;
    estTransforme = false;
    fleche = NULL;
    estTouche = false;
    attaque = false;
    tempsDebutInvincibilite = 0;
    tempsDebutAttaque = 0;

    // debutAtk = 0;

}

///Transformation du héros, à peaufiner.
void Heros::seTransforme()
{
    if(jaugeTransformation==100)
    {
        estTransforme=true;
        degatsAttaqueEpee*=2;
    }
}

void Heros::annuleTransforme()
{
    estTransforme=false;
    degatsAttaqueEpee/=2;
}

Heros::~Heros () {
    delete fleche;
    fleche = NULL;
}

///Déplacement du héros
void Heros::gauche (const Terrain & t) {
	if (t.estPositionPersoValide(x-1,y) && !attaque)
	{
        x--;
        directionVisee = "gauche";
    }
}

void Heros::droite (const Terrain & t) {
	if (t.estPositionPersoValide(x+largeur,y) && !attaque)
	{
        x++;
        directionVisee = "droite";
    }
}

void Heros::haut (const Terrain & t) {
	if (t.estPositionPersoValide(x,y+hauteur) && !attaque)
	{
        y++;
        directionVisee = "haut";
    }
}

void Heros::bas (const Terrain & t) {
	if (t.estPositionPersoValide(x,y-1) && !attaque)
	{
        y--;
        directionVisee = "bas";
    }
}


///Fonction de l'attaque à l'épée du héros
void Heros::attaqueEpee(Terrain & t)
{
    if(enVie())
    {
        if(directionVisee=="haut" && !attaque)
        {
            for(unsigned int i=x; i<x+largeur; i++)
            {
                for(unsigned int j=y+hauteur; j<y+hauteur+10; j++)
                {
                    if(t.estPositionPersoValide(i,j))
                    {
                        t.setXY(i,j,'a');
                    }
                }
                attaque=true;
                clock_t t = clock();
                tempsDebutAttaque = float(t);
            }
        }
        if(directionVisee=="bas" && !attaque)
        {

            for(unsigned int i=x; i<x+largeur; i++)
            {
                for(unsigned int j=y-1; j>y-hauteur; j--)
                {
                    if(t.estPositionPersoValide(i,j))
                    {
                        t.setXY(i,j,'a');
                    }
                }
            }
            attaque=true;
            clock_t t = clock();
            tempsDebutAttaque = float(t);
        }

        if(directionVisee=="gauche" && !attaque)
        {

            for(unsigned int i=x-1; i>x-largeur; i--)
            {
                for(unsigned int j=y; j<y+hauteur; j++)
                {
                    if(t.estPositionPersoValide(i,j))
                    {
                        t.setXY(i,j,'a');
                    }
                }
            }
            attaque=true;
            clock_t t = clock();
            tempsDebutAttaque = float(t);
        }

        if(directionVisee=="droite" && !attaque)
        {
            for(unsigned int i=x+largeur; i<x+largeur+10; i++)
            {
                for(unsigned int j=y; j<y+hauteur; j++)
                {
                    if(t.estPositionPersoValide(i,j))
                    {
                        t.setXY(i,j,'a');
                    }
                }
            }
            attaque=true;
            clock_t t = clock();
            tempsDebutAttaque = float(t);
        }
    }
}

///Fonction de l'attaque avec l'arc du héros
void Heros::attaqueArc(Terrain & t)
{
    if(!getAtk() && enVie())
    {
        if(directionVisee=="haut" && t.estPositionPersoValide(x,y+1))
        {
            if((fleche != NULL && !fleche->getEnMouvement()) | (fleche == NULL))
            {
                attaque = true;
                clock_t t = clock();
                tempsDebutAttaque = float(t);
                fleche = new Fleche(x + AJUSTEMENT_VISUEL_FLECHE,y+1 + 0,directionVisee, estTransforme);
            }
        }

        if(directionVisee=="bas" && t.estPositionPersoValide(x,y-1))
        {
            if((fleche != NULL && !fleche->getEnMouvement()) | (fleche == NULL))
            {
                attaque = true ;
                clock_t t = clock();
                tempsDebutAttaque = float(t);
                fleche = new Fleche(x + AJUSTEMENT_VISUEL_FLECHE,y-2 + 0,directionVisee, estTransforme);
            }
        }

        if(directionVisee=="gauche" && t.estPositionPersoValide(x-1,y))
        {
            if((fleche != NULL && !fleche->getEnMouvement()) | (fleche == NULL))
            {
                attaque = true;
                clock_t t = clock();
                tempsDebutAttaque = float(t);
                fleche = new Fleche(x-2 + 0,y + AJUSTEMENT_VISUEL_FLECHE,directionVisee, estTransforme);
            }
        }

        if(directionVisee=="droite" && t.estPositionPersoValide(x+1,y))
        {
            if((fleche != NULL && !fleche->getEnMouvement()) | (fleche == NULL))
            {
                attaque = true;
                clock_t t = clock();
                tempsDebutAttaque = float(t);
                fleche = new Fleche(x+1 + 0,y + AJUSTEMENT_VISUEL_FLECHE,directionVisee, estTransforme);
            }

        }
    }
}


///Collision entre le héros et les monstres.
bool Heros::collisionMonstre(const Monstre & m)
{
    unsigned int monstreX = m.getX();
    unsigned int monstreY = m.getY();
    if(m.enVie())
    {
        if((monstreX>=x+largeur)     /// le monstre ne touche pas car il est trop à droite du héros
        || (monstreX+m.getLargeur()<=x)     /// trop à gauche
        || (monstreY >= y+hauteur)   /// trop en bas
        || (monstreY+m.getHauteur() <= y))  /// trop en haut
        return false;
        else return true;
    }
    else return false;
}

///Fonction appelée lorsque le héros reçoit des dégats.
void Heros::recevoirDegats (const Terrain & t, Monstre & m)
{
    for(unsigned int i=x; i<x+largeur; i++)
    {
        for(unsigned int j=y; j<y+hauteur; j++)
        {
            if ((t.getXY(i,j)=='o') || (t.getXY(i,j)=='k'))
            {
                if(!invincible)
                {
                    pdv -= m.getDegatsAttaqueBasique();
                    m.frappe = true;
                    invincible = true;
                    clock_t t_touche = clock();
                    tempsDebutInvincibilite = float(t_touche);
                }
            }
        }
    }

    /*if(collisionMonstre(m) == true)
    {
        if(!invincible)
        {
            pdv -= m.getDegatsAttaqueBasique();
            invincible = true;
            clock_t t_touche = clock();
            tempsDebutInvincibilite = float(t_touche);
        }
    }*/
}

void Heros::recevoirDegatsProjectile(Tireur & tireur)
{
    if(tireur.getBouleDeFeu1() != NULL)
    {
        pdv -= tireur.getBouleDeFeu1()->getDegatsProjectile();

    }
    else if(tireur.getBouleDeFeu2() != NULL)
    {
        pdv -= tireur.getBouleDeFeu2()->getDegatsProjectile();
    }
    else if(tireur.getBouleDeFeu3() != NULL)
    {
        pdv -= tireur.getBouleDeFeu3()->getDegatsProjectile();
    }
}

bool Heros::collisionProjectile(const Tireur & tireur)
{
    unsigned int boxX = tireur.getBouleDeFeu1()->getHitboxX();
    unsigned int boxY = tireur.getBouleDeFeu1()->getHitboxY();
    unsigned int posX = tireur.getBouleDeFeu1()->getX();
    unsigned int posY = tireur.getBouleDeFeu1()->getY();
    if(enVie())
    {
        if((posX>=x+hitboxLargeur)     /// la flèche ne touche pas car trop à droite du heros
        || (posX+boxX<=x)     /// trop à gauche
        || (posY >= y+hitboxHauteur)   /// trop en bas
        || (posY+boxY <= y))  /// trop en haut
        return false;
        else return true;
    }
    else return false;
}
bool Heros::collisionProjectile2(const Tireur & tireur)
{
    unsigned int boxX = tireur.getBouleDeFeu1()->getHitboxX();
    unsigned int boxY = tireur.getBouleDeFeu1()->getHitboxY();
    unsigned int posX = tireur.getBouleDeFeu2()->getX();
    unsigned int posY = tireur.getBouleDeFeu2()->getY();
    if(enVie())
    {
        if((posX>=x+hitboxLargeur)     /// la flèche ne touche pas car trop à droite du heros
        || (posX+boxX<=x)     /// trop à gauche
        || (posY >= y+hitboxHauteur)   /// trop en bas
        || (posY+boxY <= y))  /// trop en haut
        return false;
        else return true;
    }
    else return false;
}
bool Heros::collisionProjectile3(const Tireur & tireur)
{
    unsigned int boxX = tireur.getBouleDeFeu1()->getHitboxX();
    unsigned int boxY = tireur.getBouleDeFeu1()->getHitboxY();
    unsigned int posX = tireur.getBouleDeFeu3()->getX();
    unsigned int posY = tireur.getBouleDeFeu3()->getY();
    if(enVie())
    {
        if((posX>=x+hitboxLargeur)     /// la flèche ne touche pas car trop à droite du heros
        || (posX+boxX<=x)     /// trop à gauche
        || (posY >= y+hitboxHauteur)   /// trop en bas
        || (posY+boxY <= y))  /// trop en haut
        return false;
        else return true;
    }
    else return false;
}

/* Lorqsue je divise le temps par CLOCKS_PER_SECONDS, les deux fonctions ne marchent pas, je ne sais pas pq, je laisse donc en ticks pour l'instant.*/
void Heros::tempsInvincible()
{
    if(invincible)
    {
        clock_t t = clock();
        if(t-tempsDebutInvincibilite > 10000)  ///Le héros est invincible 10 000 ticks après avoir reçu des dégats
        {
            invincible = false;
        }
    }
}

void Heros::tempsAttaque(Terrain & t)
{
    if(getAtk())
    {
        clock_t ta = clock();
        if(ta-tempsDebutAttaque>10000) ///Le héros ne peut pas bouger 10 000 ticks pendant son attaque.
        {
            setAtk(false);
            for(int i=0; i<t.getDimX();i++)
            {
                for(int j=0; j<t.getDimY();j++)
                {
                    if(t.getXY(i,j)=='a') t.setXY(i,j,' ');
                }
            }
        }
    }
}

void Heros::actionAuto(Terrain & t, Monstre & m)
{
    recevoirDegats(t,m);
}

void Heros::actionAuto( Terrain & t, Tireur & tireur)
{
    ///Collision entre le héros et les boules de feu
    if (tireur.getBouleDeFeu1()!= NULL)
    {
        if(collisionProjectile(tireur) && tireur.getBouleDeFeu1()->getEnMouvement())
        {
            recevoirDegatsProjectile(tireur);

            tireur.getBouleDeFeu1()->setEnMouvement(false);
            tireur.explose = true;
        }
    }
    if (tireur.getBouleDeFeu2()!= NULL)
    {
        if(collisionProjectile2(tireur) && tireur.getBouleDeFeu2()->getEnMouvement())
        {
            recevoirDegatsProjectile(tireur);
            tireur.getBouleDeFeu2()->setEnMouvement(false);
            tireur.explose = true;
        }
    }
    if (tireur.getBouleDeFeu3()!= NULL)
    {
        if(collisionProjectile3(tireur) && tireur.getBouleDeFeu3()->getEnMouvement())
        {
            recevoirDegatsProjectile(tireur);
            tireur.getBouleDeFeu2()->setEnMouvement(false);
            tireur.explose = true;
        }
    }
}

void Heros::actionAuto(Terrain & t)
{
    tempsInvincible();
    tempsAttaque(t);
    if(getTransforme())
    {
        setJaugeTransformation(getJaugeTransformation()-1);
        if(getJaugeTransformation()==0) annuleTransforme();

    }


     ///tire la flèche en fonction de la direction où regarde le héros.
    if (getFleche() != NULL)
    {
        if(getFleche()->getDirectionProjectile()=="haut" && getFleche()->getEnMouvement())
        {
            getFleche()->haut(t);
        }
        if(getFleche()->getDirectionProjectile()=="bas" && getFleche()->getEnMouvement())
        {
            getFleche()->bas(t);
        }
        if(getFleche()->getDirectionProjectile()=="gauche" && getFleche()->getEnMouvement())
        {
            getFleche()->gauche(t);
        }
        if(getFleche()->getDirectionProjectile()=="droite" && getFleche()->getEnMouvement())
        {
            getFleche()->droite(t);
        }
    }
}

std::string Heros::getDirectionVisee() const {return directionVisee;}
int Heros::getDegatsAttaqueEpee() const {return degatsAttaqueEpee;}
Fleche* Heros::getFleche() const {return fleche;}

void Heros::detruireFleche () {
    delete fleche;
    fleche = NULL;
}

unsigned int Heros::getJaugeTransformation() const {return jaugeTransformation;}

void Heros::setJaugeTransformation(unsigned int i)
{
    if (i<100) jaugeTransformation = i;
    else jaugeTransformation = 100;}

bool Heros::getTransforme() const { return estTransforme;}
bool Heros::getAtk(){return attaque;}
void Heros::setAtk(bool b){attaque=b;}
void Heros::setX(int posX) { x = posX;}
void Heros::setY(int posY) { y = posY;}
bool Heros::getInvincible(){return invincible;}
void Heros::setInvincible(bool b){invincible = b;}
