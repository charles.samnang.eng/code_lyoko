#include "Jeu.h"
#include <time.h>

#include <iostream>
#include <fstream>

using namespace std;

Jeu::Jeu (int lvl) : ter(), her(), vag(0) {

	variableTest = 1;
}

Jeu::~Jeu () {

}


Terrain& Jeu::getTerrain () { return ter; }

Heros& Jeu::getHeros () {	return her; }

Vague& Jeu::getVague() { return vag;}

/*Troll& Jeu::getTroll () { return tro; }

Gobelin& Jeu::getGobelin () { return gob; }

Sanglier& Jeu::getSanglier () { return san; }

Scarabee& Jeu::getScarabee () { return sca; }

Tireur& Jeu::getTireur () { return tir; }



Pierre& Jeu::getPierre () { return pie; }

Feu& Jeu::getFeu () { return feu; }*/






const Terrain& Jeu::getConstTerrain () const { return ter; }

const Heros& Jeu::getConstHeros () const { return her; }

const Vague& Jeu::getConstVague() const { return vag;}

/*const Troll& Jeu::getConstTroll () const { return tro; }

const Gobelin& Jeu::getConstGobelin () const { return gob; }

const Sanglier& Jeu::getConstSanglier () const { return san; }

const Scarabee& Jeu::getConstScarabee () const { return sca; }

const Tireur& Jeu::getConstTireur () const { return tir; }



const Pierre& Jeu::getConstPierre () const { return pie; }

const Feu& Jeu::getConstFeu () const { return feu; }*/

Heros Jeu::getHeros () const {return her;}

int Jeu::getNombreDeFantome() const { return 1; } // à modifier ou supprimer selon si la base peut être utile ou non


void Jeu::actionClavier (const char touche) {
	switch(touche) {
		case 'g' :
				her.gauche(ter);
				break;
		case 'd' :
				her.droite(ter);
				break;
		case 'h' :
				her.haut(ter);
				break;
		case 'b' :
				her.bas(ter);
				break;
		case 'e' :
				her.attaqueEpee(ter);
				break;
		case 'a' :
				her.attaqueArc(ter);
				break;
		case 's' :
				her.seTransforme();
				break;

		//la procédure pause doit avoir un (ou plusieurs) paramètre(s) ? Il ne doit pas y avoir de break ? Chercher comment implémenter la fonctionnalité pause. --> En fait essayer de lier la touche p à Ctrl + Z, qui met en pause l'exécution du programme
		case 'p' :
				//pause(); // !!!ATTENTION pause n'est pas une procédure membre de la classe Heros, mais de la classe Jeu ! Il faudra donc l'implémenter dans la classe Jeu
				break;

		case 't' :
				//her.tueTous(ter);
				break;

	}



}

void Jeu::actionsAutomatiques () {

    if(vag.getNbMonstresVivant()==0)
    {
        //if (her.getFleche() != NULL && her.getFleche()->getEnMouvement())
            //delete her.getFleche(); !!! S'EN OCCUPER (pour que flèche soit supprimée à passage à autre vague)
        lvl++;
        vag = Vague(lvl);
        her.setX(77);
        her.setY(55);
    }
    her.actionAuto(ter);
    for(int i=0; i<vag.getNbGobelin(); i++)
    {
        vag.getTabGobelin()[i].actionAuto(ter,her);
        vag.getTabGobelin()[i].versHeros(ter,her);
        vag.getTabGobelin()[i].coupDePetiteMassue(ter,her);
        vag.getTabGobelin()[i].actionAuto2(ter,her);
        her.actionAuto(ter, vag.getTabGobelin()[i]);
    }

    for(int i=0; i<vag.getNbTroll(); i++)
    {
        vag.getTabTroll()[i].actionAuto(ter,her);
        vag.getTabTroll()[i].versHeros(ter,her);
        vag.getTabTroll()[i].coupDeMassue(ter,her);
        vag.getTabTroll()[i].actionAuto2(ter,her);
        her.actionAuto(ter, vag.getTabTroll()[i]);
    }

    for(int i=0; i<vag.getNbTireur(); i++)
    {
        vag.getTabTireur()[i].tirerProjectile(her);
        vag.getTabTireur()[i].deplacementProjectile(ter);
        vag.getTabTireur()[i].bougeAuto(ter);
        vag.getTabTireur()[i].actionAuto(ter,her);
        her.actionAuto(ter, vag.getTabTireur()[i]);
    }
    //Attaque automatique du troll sur le héros : j'ai (Raph) pas encore eu le temps de finir cette section
  //  tro.coupDeMassue(ter,her);
    ///her.recevoirDegats(ter,tro); // à mettre dans coupDeMassue ?
}
