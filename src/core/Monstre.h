/**
@brief Module gérant un monstre
@file Monstre.h
@date 2021/03/16
*/

#ifndef _MONSTRE_H
#define _MONSTRE_H

#include "Terrain.h"
#include "Personnage.h"
#include "Heros.h"
//#include "Jeu.h"

#include <ctime>

/**
@brief Un monstre = sa position 2D
*/

class Heros;

class Monstre : public Personnage {

protected :

	int dir;

    bool enMouvement;
    bool attaque;
    int degatsAttaqueBasique;
    int porteAttaqueBasique;

    /// Pour des raisons de simplicité d'implémentation, plus vitesseAttaque a une valeur élevée, plus cette vitesse est en réalité petite.
    float vitesseAttaque;

    /// Pour des raisons de simplicité d'implémentation, plus vitesseDeplacement a une valeur élevée, plus cette vitesse est en réalité petite.
    float vitesseDeplacement;
    
    bool intouchable;
    clock_t tempsEnClicks;
    double tempsEnSec; // ou float ? (unsigned) int ? revoir LIFAP3/LIFAP4
    float tempsDebutIntouchable;

public:

    bool seFaitTrancher, seFaitTranspercer, frappe;

    void gauche (const Terrain & t);
    void droite (const Terrain & t);
    void haut (const Terrain & t);
    void bas (const Terrain & t);

    //void bougeAuto (const Terrain & t); //IA. Raph doit la coder

    void recevoirDegatsEpee(const Terrain & t, Heros & h);

    void recevoirDegatsFleche(Heros & h);
    bool collisionFleche(const Heros & h);
    void tempsIntouchable();


    int getDir () const;

    bool getEnMouvement () const;
    bool getAttaque () const;
    int getDegatsAttaqueBasique () const;
    int getPorteAttaqueBasique () const;
    float getVitesseAttaque () const;
    float getVitesseDeplacement () const;
    std::string getDirectionVisee() const;

    void actionAuto(Terrain & t, Heros & h);

    clock_t getTempsEnClicks () const;
    void setTempsEnClicks (const clock_t & t);

    double getTempsEnSec () const;
    void setTempsEnSec (const double & t);

    void setIntouchable(const bool & b);
    void setPdv(const int & vie);
};

#endif
