#include "Monstre.h"
#include "Heros.h"
#include <stdlib.h>

void Monstre::gauche (const Terrain & t) {
	if (t.estPositionPersoValide(x-1,y)) x--;
}

void Monstre::droite (const Terrain & t) {
	if (t.estPositionPersoValide(x+1,y)) x++;
}

void Monstre::haut (const Terrain & t) {
	if (t.estPositionPersoValide(x,y+1)) y++;
}

void Monstre::bas (const Terrain & t) {
	if (t.estPositionPersoValide(x,y-1)) y--;
}

/*void Monstre::bougeAuto (const Terrain & t) {
    int dx [4] = { 1, 0, -1, 0};
    int dy [4] = { 0, 1, 0, -1};
    int xtmp,ytmp;
    xtmp = x + dx[dir];
    ytmp = y + dy[dir];
    if (t.estPositionPersoValide(xtmp,ytmp)) {
        x = xtmp;
        y = ytmp;
    }
    else dir = rand()%4;
}*/



void Monstre::recevoirDegatsEpee (const Terrain & t, Heros & h)
{

    for(unsigned int i=x;i<x+largeur-4;i++)
    {
        for(unsigned int j=y;j<y+hauteur-4;j++)
        {
            if(t.getXY(i,j)=='a' && intouchable==false)
            {
                pdv -= h.getDegatsAttaqueEpee();
                seFaitTrancher = true;
                intouchable = true;
                if(!h.getTransforme()) h.setJaugeTransformation(h.getJaugeTransformation()+5);
                clock_t t_touche = clock();
                tempsDebutIntouchable = float(t_touche);
            }
        }
    }
}

void Monstre::tempsIntouchable()
{
    if(intouchable)
    {
        clock_t t = clock();
        if(t-tempsDebutIntouchable > 35000)  ///Le héros est invincible 10 000ticks après avoir reçu des dégats
        {
            intouchable = false;
        }
    }
}

void Monstre::recevoirDegatsFleche(Heros & h)
{
    pdv -= h.getFleche()->getDegatsProjectile();
    seFaitTranspercer = true;
    if(!h.getTransforme()) h.setJaugeTransformation(h.getJaugeTransformation()+2);
}

bool Monstre::collisionFleche(const Heros & h)
{
    unsigned int boxX = h.getFleche()->getHitboxX();
    unsigned int boxY = h.getFleche()->getHitboxY();
    unsigned int posX = h.getFleche()->getX();
    unsigned int posY = h.getFleche()->getY();
    if(enVie())
    {
        if((posX>=x+hitboxLargeur)     /// la flèche ne touche pas car trop à droite du monstre
        || (posX+boxX<=x)     /// trop à gauche
        || (posY >= y+hitboxHauteur)   /// trop en bas
        || (posY+boxY <= y))  /// trop en haut
        return false;
        else return true;
    }
    else return false;
}


int Monstre::getDir () const {
    return dir;
}


bool Monstre::getEnMouvement () const {
    return enMouvement;
}

bool Monstre::getAttaque () const {
    return attaque;
}

int Monstre::getDegatsAttaqueBasique () const {
    return degatsAttaqueBasique;
}

int Monstre::getPorteAttaqueBasique () const {
    return porteAttaqueBasique;
}

float Monstre::getVitesseAttaque () const {
    return vitesseAttaque;
}

float Monstre::getVitesseDeplacement () const {
    return vitesseDeplacement;
}

std::string Monstre::getDirectionVisee() const {return directionVisee;}

void Monstre::actionAuto( Terrain & t, Heros & h)
{
    ///Collision entre le monstre et les flèches du héros
    if (h.getFleche() != NULL)
    {
        if(collisionFleche(h) && h.getFleche()->getEnMouvement())
        {
            recevoirDegatsFleche(h);
            h.getFleche()->setEnMouvement(false);
            h.detruireFleche();
        }
    }
    recevoirDegatsEpee(t,h);
    tempsIntouchable();
}

clock_t Monstre::getTempsEnClicks () const { return tempsEnClicks; }

void Monstre::setTempsEnClicks (const clock_t & t) { tempsEnClicks = t; }

double Monstre::getTempsEnSec () const { return tempsEnSec; }

void Monstre::setTempsEnSec (const double & t) { tempsEnSec = t; }

void Monstre::setIntouchable(const bool & b) { intouchable = b; }

void Monstre::setPdv(const int & vie) { pdv = vie;}
