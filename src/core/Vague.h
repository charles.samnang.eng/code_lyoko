// L'objet Vague ne va qu'être créé au début de la partie et détruit en fin de partie. Il n'y aura donc qu'une seule vague, qui sera modifiée au cours du jeu.

// Peut-être préciser utilisation nbMonstres en coms et en implémentation

#ifndef _VAGUE_H
#define _VAGUE_H

#include "Troll.h"
#include "Gobelin.h"
#include "Tireur.h"
/*#include "Sanglier.h"
#include "Scarabee.h"
#include "Fleche.h"
#include "Pierre.h"
#include "Feu.h"*/

class Vague {
    private:

        /// Nombre de monstres du niveau (avant que le héros n'en tue)
        int nbMonstres;

        /// Nombre de trolls du niveau (avant que le héros n'en tue)
        int nbTroll;

        /// Nombre de gobelins du niveau (avant que le héros n'en tue)
        int nbGobelin;

        /// Nombre de tireurs du niveau (avant que le héros n'en tue)
        int nbTireur;
        
        Troll* tabTroll;
        Gobelin* tabGobelin;
        Tireur* tabTireur;

        public:
        Vague(int lvl);
        ~Vague();
        Troll* getTabTroll() const;
        Gobelin* getTabGobelin() const;
        Tireur* getTabTireur() const;
        int getNbMonstres() const;
        int getNbTroll() const;
        int getNbGobelin() const;
        int getNbTireur() const;
        int getNbMonstresVivant() const;
};



#endif
