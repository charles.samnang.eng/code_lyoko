#ifndef _PERSONNAGE_H
#define _PERSONNAGE_H

#include <string>
#include <stdlib.h>
#include <unistd.h>

class Personnage
{
    protected :  ///protected au lieu de private afin que les classes filles telles que Heros et Monstre accèdent plus facilement aux données de la classe Personnage.
        int pdv;
        unsigned int x;
        unsigned int y;
        std::string directionVisee;
        int directionViseeX;
        int directionViseeY;
        unsigned int hauteur;
        unsigned int largeur;
        unsigned int hitboxHauteur;
        unsigned int hitboxLargeur;

    public:

    bool vientDeMourir;

    //Personnage();
    //~Personnage();
    bool enVie () const;

    unsigned int getX() const;
    unsigned int getY() const;
    unsigned int getLargeur() const;
    unsigned int getHauteur() const;

};


#endif // _PERSONNAGE_H
