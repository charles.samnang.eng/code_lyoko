#ifndef _PROJECTILE_H
#define _PROJECTILE_H

#include <string>
#include <stdlib.h>
#include "Terrain.h"
#include <unistd.h>

class Projectile
{
    protected:
        unsigned int x;
        unsigned int y;

        /// Equivalent de largeur de Personnage
        unsigned int dimx;

        /// Equivalent de hauteur de Personnage
        unsigned int dimy;

        /// Equivalent de hitboxLargeur de Personnage
        unsigned int hitboxX;

        /// Equivalent de hitboxHauteur de Personnage
        unsigned int hitboxY;
        
        bool enMouvement;
        unsigned int degatsProjectile;
        std::string directionProjectile;
        float vitesseProjectile;
        float tempsDernierDeplacement;

    public:
        void toucheTerrain();
        void touchePersonnage();

        void gauche (const Terrain & t);
        void droite (const Terrain & t);
        void haut (const Terrain & t);
        void bas (const Terrain & t);

        unsigned int getDegatsProjectile() const;
        std::string getDirectionProjectile() const;

        unsigned int getX() const;
        unsigned int getY() const;
        unsigned int getDimx() const;
        unsigned int getDimy() const;
        unsigned int getHitboxX() const;
        unsigned int getHitboxY() const;
        bool getEnMouvement() const;
        void setEnMouvement(bool b);
};

#endif // _PROJECTILE_H
