#include "Tireur.h"
#include <math.h>

Tireur::Tireur()
{}

Tireur::Tireur(unsigned int posX, unsigned int posY) {

// Données membres de Personnage
    pdv = 0;
    x = posX;
    y = posY;
    directionVisee = "droite"; // "bas" remplacé par "droite" récemment
    directionViseeX = 0;
    directionViseeY = 0;
    hauteur = 6;
    largeur = 6;
    hitboxHauteur = 4;
    hitboxLargeur = 5;
    vientDeMourir = false;


// Données membres de Monstre
    dir = 0;
    enMouvement = false;
    attaque = false;
    degatsAttaqueBasique = 8;
    porteAttaqueBasique = 200;
    vitesseAttaque = 10; ///pas encore sûr, à définir
    vitesseDeplacement = 3; ///pas encore sûr, à définir
    tempsEnClicks = 0;
    tempsEnSec = 0;
    intouchable = false;
    seFaitTrancher = false;
    seFaitTranspercer = false;
    

// Données membres de Tireur
    tempsDernierTir = 0;
    fireball1 = NULL;
    fireball2 = NULL;
    fireball3 = NULL;

    tire = false;
    explose = false;

}

bool Tireur::herosAPortee(const Heros & h)
{
    if(x+largeur/2>=h.getX() && x+largeur/2<h.getX()+h.getLargeur() && h.getY()-hauteur-y<porteAttaqueBasique && h.getY()-hauteur-y>0)
    {
        directionVisee = "haut";
        return true;
    }
    if(x+largeur/2>=h.getX() && x+largeur/2<h.getX()+h.getLargeur() && y-h.getY()<porteAttaqueBasique && y-h.getY()>0)
    {
        directionVisee = "bas";
        return true;
    }
    if(y+hauteur/2>=h.getY() && y+hauteur/2<h.getY()+h.getHauteur() && h.getX()-largeur-x<porteAttaqueBasique && h.getX()-largeur-x>0)
    {
        directionVisee = "droite";
        return true;
    }
    if(y+hauteur/2>=h.getY() && y+hauteur/2<h.getY()+h.getHauteur() && x-h.getX()<porteAttaqueBasique && x-h.getX()-x>0)
    {
        directionVisee = "gauche";
        return true;
    }
    return false;
}

void Tireur::tirerProjectile(const Heros & h)
{
    clock_t tActuel = clock();
    if(herosAPortee(h) && tActuel-tempsDernierTir>10000 && enVie())
    {
        if(directionVisee == "haut")
        {
             if((fireball1 == NULL) | (fireball1 != NULL && !fireball1->getEnMouvement()))
            {
                fireball1 = new BouleDeFeu((x+3), (y+hauteur), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
            else if(fireball2 == NULL || (fireball2 !=NULL && !fireball2->getEnMouvement()))
            {
                fireball2 = new BouleDeFeu((x+3), (y+hauteur), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
            else if(fireball3 == NULL || (fireball3 !=NULL && !fireball3->getEnMouvement()))
            {
                fireball3 = new BouleDeFeu((x+3), (y+hauteur), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
        }
         if(directionVisee =="bas")
        {
             if((fireball1 == NULL) | (fireball1 != NULL && !fireball1->getEnMouvement()))
            {
                fireball1 = new BouleDeFeu((x+3), (y-1), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
            else if(fireball2 == NULL || (fireball2 !=NULL && !fireball2->getEnMouvement()))
            {
                fireball2 = new BouleDeFeu((x+3), (y-1), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
            else if(fireball3 == NULL || (fireball3 !=NULL && !fireball3->getEnMouvement()))
            {
                fireball3 = new BouleDeFeu((x+3), (y-1), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
        }
         if(directionVisee == "gauche")
        {
             if((fireball1 == NULL) | (fireball1 != NULL && !fireball1->getEnMouvement()))
            {
                fireball1 = new BouleDeFeu(x-1, (y+3), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
            else if(fireball2 == NULL || (fireball2 !=NULL && !fireball2->getEnMouvement()))
            {
                fireball2 = new BouleDeFeu(x-1, (y+3), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
            else if(fireball3 == NULL || (fireball3 !=NULL && !fireball3->getEnMouvement()))
            {
                fireball3 = new BouleDeFeu(x-1, (y+3), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
        }
        if(directionVisee == "droite")
        {
            if((fireball1 == NULL) | (fireball1 != NULL && !fireball1->getEnMouvement()))
            {
                fireball1 = new BouleDeFeu((x+largeur), (y+3), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
            else if(fireball2 == NULL || (fireball2 !=NULL && !fireball2->getEnMouvement()))
            {
                fireball2 = new BouleDeFeu((x+largeur), (y+3), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
            else if(fireball3 == NULL || (fireball3 !=NULL && !fireball3->getEnMouvement()))
            {
                fireball3 = new BouleDeFeu((x+largeur), (y+3), directionVisee);
                tire = true;
                clock_t t = clock();
                tempsDernierTir = float(t);
            }
        }
    }
}

void Tireur::deplacementProjectile(const Terrain & t)
{
    if(getBouleDeFeu1() != NULL)
    {
        if(getBouleDeFeu1()->getDirectionProjectile()=="haut" && getBouleDeFeu1()->getEnMouvement())
        {
            getBouleDeFeu1()->haut(t);
        }
        if(getBouleDeFeu1()->getDirectionProjectile()=="bas" && getBouleDeFeu1()->getEnMouvement())
        {
            getBouleDeFeu1()->bas(t);
        }
        if(getBouleDeFeu1()->getDirectionProjectile()=="gauche" && getBouleDeFeu1()->getEnMouvement())
        {
            getBouleDeFeu1()->gauche(t);
        }
        if(getBouleDeFeu1()->getDirectionProjectile()=="droite" && getBouleDeFeu1()->getEnMouvement())
        {
            getBouleDeFeu1()->droite(t);
        }
    }
    if(getBouleDeFeu2() != NULL)
    {
        if(getBouleDeFeu2()->getDirectionProjectile()=="haut" && getBouleDeFeu2()->getEnMouvement())
        {
            getBouleDeFeu2()->haut(t);
        }
        if(getBouleDeFeu2()->getDirectionProjectile()=="bas" && getBouleDeFeu2()->getEnMouvement())
        {
            getBouleDeFeu2()->bas(t);
        }
        if(getBouleDeFeu2()->getDirectionProjectile()=="gauche" && getBouleDeFeu2()->getEnMouvement())
        {
            getBouleDeFeu2()->gauche(t);
        }
        if(getBouleDeFeu2()->getDirectionProjectile()=="droite" && getBouleDeFeu2()->getEnMouvement())
        {
            getBouleDeFeu2()->droite(t);
        }
    }
    if(getBouleDeFeu3() != NULL)
    {
        if(getBouleDeFeu3()->getDirectionProjectile()=="haut" && getBouleDeFeu3()->getEnMouvement())
        {
            getBouleDeFeu3()->haut(t);
        }
        if(getBouleDeFeu3()->getDirectionProjectile()=="bas" && getBouleDeFeu3()->getEnMouvement())
        {
            getBouleDeFeu3()->bas(t);
        }
        if(getBouleDeFeu3()->getDirectionProjectile()=="gauche" && getBouleDeFeu3()->getEnMouvement())
        {
            getBouleDeFeu3()->gauche(t);
        }
        if(getBouleDeFeu3()->getDirectionProjectile()=="droite" && getBouleDeFeu3()->getEnMouvement())
        {
            getBouleDeFeu3()->droite(t);
        }
    }
}

void Tireur::bougeAuto (const Terrain & t) {
    int dx [4] = { 1, 0, -1, 0};
    int dy [4] = { 0, 1, 0, -1};
    int xtmp,ytmp;
    xtmp = x + dx[dir];
    ytmp = y + dy[dir];
    if(dir == 0)
    {
        if (t.estPositionPersoValide(xtmp+largeur,ytmp)) {
        x = xtmp;
    }
        else dir=rand()%4;
    }
    else if(dir == 1)
    {
        if (t.estPositionPersoValide(xtmp,ytmp+hauteur)) {
        y = ytmp;
    }
        else dir=rand()%4;
    }
    else if (t.estPositionPersoValide(xtmp,ytmp)) {
        x = xtmp;
        y = ytmp;
    }
    else dir = rand()%4;
}


BouleDeFeu* Tireur::getBouleDeFeu1() const {return fireball1;}
BouleDeFeu* Tireur::getBouleDeFeu2() const {return fireball2;}
BouleDeFeu* Tireur::getBouleDeFeu3() const {return fireball3;}
