#ifndef _FLECHE_H
#define _FLECHE_H

#include "Projectile.h"

class Fleche : public Projectile
{

    public :
        Fleche(unsigned int posX, unsigned int posY, std::string str, bool etatTransformation);
        void gauche (const Terrain & t);
        void droite (const Terrain & t);
        void haut (const Terrain & t);
        void bas (const Terrain & t);

};

#endif // _FLECHE_H
