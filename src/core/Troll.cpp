#include "Troll.h"
#include <stdlib.h>
#include <cmath>
#include <time.h>

#include <iostream>
#include <fstream>

using namespace std;

//Procédure qui permet de débuguer
void ecritureDansFichier (string i, float a) {
    ofstream ResolutionAttaqueTroll;
	ResolutionAttaqueTroll.open("Debug_et_tests/Attaque_troll.txt", ios::app);
	if (ResolutionAttaqueTroll.is_open ()) {
		ResolutionAttaqueTroll << i << a << endl;
		ResolutionAttaqueTroll.close();
	}
	else {
		cout << "Impossible d'ouvrir le fichier" << endl;
	}
}

Troll::Troll()
{}

Troll::Troll (int _x, int _y) {

// Données membres de Personnage
    pdv = 0;
    x = _x;
    y = _y;
    hauteur = 16;
    largeur = 16;
    directionVisee = "droite";
    directionViseeX = 0;
    directionViseeY = 0;
    hitboxHauteur = 16;
    hitboxLargeur = 16;
    vientDeMourir = false;


// Données membres de Monstre
	dir = 0;
    enMouvement = false;
    attaque = false;
    degatsAttaqueBasique = 5; // avant c'était 7, et encore avant c'était 17
    porteAttaqueBasique = 3;
    vitesseAttaque = 4;
    vitesseDeplacement = 5;
    tempsEnClicks = 0;
    tempsEnSec = 0;
    intouchable = false;
    seFaitTrancher = false;
    seFaitTranspercer = false;


// Données membres de Troll
    tempsDernierTraitement = 0;
    tempsDernierTraitement2 = 0;
    variableTest = 1;

    attaque = false;
    tempsDebutAttaque = 0;
    frappe = false;

}


void Troll::versHeros (const Terrain & t, const Heros & h) {

    clock_t te = clock();
	float teS = ((float)te/CLOCKS_PER_SEC);

    if (teS - tempsDernierTraitement > vitesseDeplacement/1000) {

        int dx = h.getX()-x;
        int dy = h.getY()-y;
        std::string direction = "droite";
        if (dx>0) {
            dx=1;
            direction = "droite";
        }
        if (dx<0) {
            dx=-1;
            direction = "gauche";
        }
        if (dy>0) {
            dy=1;
        }
        if (dy<0) {
            dy=-1;
        }

        if (

            /* Chantier en cours, pas encore eu le temps de finir (Raph). A la place des 1 et 2, c'est une donnée membre sur la portée de l'attaque du
            Troll qui sera utilisée. En effet, un monstre qui peut attaquer à 2 cases de distance au lieu d'1 ne cherchera pas à s'avancer à une case du
            héros.*/

            t.estPositionPersoValide(x+dx,y+dy)
            && !(h.getX()==getX() && (h.getY()==getY()-1 /*|| h.getY()==getY()-2*/))
            && !(h.getX()==getX() && (h.getY()==getY()+1 /*|| h.getY()==getY()+2*/))
            && !(h.getY()==getY() && (h.getX()==getX()-1 /*|| h.getX()==getX()-2*/))
            && !(h.getY()==getY() && (h.getX()==getX()+1 /*|| h.getX()==getX()+2*/))

            && !(h.getX()==getX()+1 && h.getY()==getY()+1)
            && !(h.getX()==getX()+1 && h.getY()==getY()-1)
            && !(h.getX()==getX()-1 && h.getY()==getY()+1)
            && !(h.getX()==getX()-1 && h.getY()==getY()-1)
            ) {
            x = x+dx;
            y = y+dy;
            if (direction == "droite") directionVisee = "droite";
            else directionVisee = "gauche";
        }

        tempsDernierTraitement = teS;

    }

}



void Troll::coupDeMassue (Terrain & t, const Heros & h) {

    if (pdv > 0) {

        float absHeros = ((float) h.getX());
        float ordHeros = ((float) h.getY());
        float absTroll = ((float) x);
        float ordTroll = ((float) y);

        float distanceTrollHeros = sqrt(pow(absHeros-absTroll,2)+pow(ordHeros-ordTroll,2));

        float portee = ((float) porteAttaqueBasique);

        clock_t te2 = clock();
        float teS2 = ((float)te2/CLOCKS_PER_SEC)*100;

        if (teS2 - tempsDernierTraitement2 > vitesseAttaque) {

            if(x <= h.getX() && y > h.getY()) {

                if (distanceTrollHeros - hitboxLargeur <= portee) {

                    for(unsigned int j=y-porteAttaqueBasique; j<y; j++)
                    {
                        for(unsigned int i=x; i<=x+porteAttaqueBasique; i++) {
                            t.setXY(i,j,'o');
                        }

                    }
                    attaque=true;
                    clock_t t = clock();
                    tempsDebutAttaque = float(t);

                }

            }


            if(x < h.getX() && y <= h.getY()) {

                if (distanceTrollHeros - hitboxLargeur <= portee) {

                    for(unsigned int j=y; j<=y+porteAttaqueBasique; j++)
                    {
                        for(unsigned int i=x+1; i<=x+porteAttaqueBasique; i++) {
                            t.setXY(i,j,'o');
                        }

                    }
                    attaque=true;
                    clock_t t = clock();
                    tempsDebutAttaque = float(t);

                }

            }


            if(x >= h.getX() && y < h.getY()) {

                if (distanceTrollHeros - hitboxLargeur <= portee) {

                    for(unsigned int j=y+1; j<=y+porteAttaqueBasique; j++)
                    {
                        for(unsigned int i=x-porteAttaqueBasique; i<=x; i++) {
                            t.setXY(i,j,'o');
                        }

                    }
                    attaque=true;
                    clock_t t = clock();
                    tempsDebutAttaque = float(t);

                }

            }


            if(x > h.getX() && y >= h.getY()) {

                if (distanceTrollHeros - hitboxLargeur <= portee) {

                    for(unsigned int j=y-porteAttaqueBasique; j<=y; j++)
                    {
                        for(unsigned int i=x-porteAttaqueBasique; i<x; i++) {
                            t.setXY(i,j,'o');
                        }

                    }
                    attaque=true;
                    clock_t t = clock();
                    tempsDebutAttaque = float(t);

                }

            }


            tempsDernierTraitement2 = teS2;
            

        }

    }

}


/*void Troll::lancerDePierre (const Pierre & pie) const {

}*/


void Troll::tempsAttaque(Terrain & t, Heros & h)
{
    if(getAtk())
    {
        clock_t ta = clock();
        if(ta-tempsDebutAttaque>5000)
        {
            setAtk(false);
            for(int i=0; i<t.getDimX();i++)
            {
                for(int j=0; j<t.getDimY();j++)
                {
                    if(t.getXY(i,j)=='o') t.setXY(i,j,' ');
                }
            }
        }
    }
}


bool Troll::getAtk(){return attaque;}
void Troll::setAtk(bool b){attaque=b;}


void Troll::actionAuto2(Terrain & t, Heros & h)
{
    tempsAttaque(t,h);
}