
// !!! Version pour le mode SDL !!!
//

#include "Terrain.h"

#include <cassert>
#include <iostream>

using namespace std;


// fait 155 caractères de longueur (de gauche à droite) et 70 caractères de largeur (de haut en bas)
const char terrain2[70][156] = {
 "###########################################################################################################################################################",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "#                                                                                                                                                         #",
 "###########################################################################################################################################################"
};


Terrain::Terrain () {
	dimx = 155;
	dimy = 70;
	for(int x=0;x<dimx;++x) {
		for(int y=0;y<dimy;++y)
			ter[x][y] = terrain2[dimy-1-y][x];
		ter[x][dimy] = '\0';
	}
}

Terrain::~Terrain () {
	for(int x=0;x<dimx;++x)
		for(int y=0;y<dimy;++y)
			ter[x][y] = '0';
	dimx = -1;
	dimy = -1;
}

bool Terrain::estPositionPersoValide (const int x, const int y) const {
	return ((x>=4) && (x<dimx) && (y>=4) && (y<dimy) && (ter[x][y]!='#'));
}

char Terrain::getXY (const int x, const int y) const {
	assert(x>=0);
	assert(y>=0);
	assert(x<dimx);
	assert(y<dimy);
	return ter[x][y];
}

int Terrain::getDimX () const { return dimx; }

int Terrain::getDimY () const {	return dimy; }



void Terrain::setXY (const int x, const int y, char objet) {
	assert(x>=0);
	assert(y>=0);
	assert(x<dimx);
	assert(y<dimy);
	ter[x][y]=objet;
}

//implémentation peut-être incomplète mais procédure peut-être pas nécessaire
void Terrain::setDimX (const int & x) {
	dimx = x;
}

//implémentation peut-être incomplète mais procédure peut-être pas nécessaire
void Terrain::setDimY (const int & y) {
	dimy = y;
}

void Terrain::effacerTerrain(unsigned int x, unsigned int y)
{
    setXY(x,y,' ');
}

void Terrain::testRegression () {

	cout << "Debut du test de regression de Terrain" << endl;

	Terrain unTerrain;
	cout << "Terrain unTerrain cree" << endl;

	assert(unTerrain.dimx==155 && unTerrain.dimy==70);
	cout << "unTerrain fait " << unTerrain.dimx << " caractères de longueur (de gauche à droite) et " << unTerrain.dimy << " caractères de largeur (de haut en bas)" << endl;

	for(int x=0; x < unTerrain.dimx; ++x)
		for(int y=0; y < unTerrain.dimy; ++y)
			assert(unTerrain.ter[x][y] == terrain2[unTerrain.dimy-1-y][x]);
	cout << "unTerrain est parfaitement identique à terrain2" << endl;

	assert(unTerrain.estPositionPersoValide(39,34));

	assert(!unTerrain.estPositionPersoValide(0,15));
	assert(!unTerrain.estPositionPersoValide(41,0));
	assert(!unTerrain.estPositionPersoValide(unTerrain.dimx-1,17));
	assert(!unTerrain.estPositionPersoValide(66,unTerrain.dimy-1));

	assert(!unTerrain.estPositionPersoValide(unTerrain.dimx,19));
	assert(!unTerrain.estPositionPersoValide(61,unTerrain.dimy));
	assert(!unTerrain.estPositionPersoValide(59,-1));
	assert(!unTerrain.estPositionPersoValide(-1,21));

	cout << "La fonction membre estPositionPersoValide permet bien de savoir si une case est une case du terrain qui n'est pas un mur, ou non" << endl;

	assert(unTerrain.getXY (44,5) == ' ');
	assert(unTerrain.getXY (26,unTerrain.dimy-1) == '#');
	cout << "La fonction membre getXY permet bien de récupérer le caractère contenu dans une case du terrain (' ' ou '#') dont on indique les coordonnées" << endl;

	assert(unTerrain.getDimX () == unTerrain.dimx);
	cout << "La fonction membre getDimX permet bien de récupérer la longueur (de gauche à droite) du terrain" << endl;
	assert(unTerrain.getDimY () == unTerrain.dimy);
	cout << "La fonction membre getDimY permet bien de récupérer la largeur (de haut en bas) du terrain" << endl;

	unTerrain.setXY (132,38,'I');
	assert(unTerrain.getXY (132,38) == 'I');
	cout << "La procédure membre setXY permet bien de remplacer un caractère contenu dans une case du terrain (dont on indique les coordonnées) par un autre caractère" << endl;

	cout << "Fin du test de regression de Terrain, tout est OK !" << endl << endl << endl << endl << endl;

}
