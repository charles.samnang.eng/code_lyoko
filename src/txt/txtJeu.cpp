#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif // WIN32
#include "winTxt.h"

#include "../core/Jeu.h"

void txtAff(WinTXT & win, const Jeu & jeu) {
	const Terrain& ter = jeu.getConstTerrain();
	const Heros& her = jeu.getConstHeros();
	const Vague& vag = jeu.getConstVague();
	/*const Sanglier& san = jeu.getConstSanglier();
	const Scarabee& sca = jeu.getConstScarabee();
	const Tireur& tir = jeu.getConstTireur();

	const Pierre& pie = jeu.getConstPierre();
	const Feu& feu = jeu.getConstFeu();*/

	win.clear();

    // Affichage des murs
	for(int x=0;x<ter.getDimX();++x)
		for(int y=0;y<ter.getDimY();++y)
			win.print(x,y,ter.getXY(x,y));

    // Affichage du Héros
	for(unsigned int x=her.getX(); x<her.getX()+her.getLargeur(); ++x)
    {
        for(unsigned int y=her.getY(); y<her.getY()+her.getHauteur();++y)
        {
            if(her.enVie())
            {
                if(her.getTransforme()==true)
                {
                    win.print(x,y,'S');
                }
                else win.print(x,y,'H');
            }

        }
    }

    for(int i=0; i<vag.getNbTroll(); i++)
    {
        for(unsigned int x=vag.getTabTroll()[i].getX(); x<vag.getTabTroll()[i].getX()+vag.getTabTroll()[i].getLargeur(); ++x)
        {
            for(unsigned int y=vag.getTabTroll()[i].getY(); y<vag.getTabTroll()[i].getY()+vag.getTabTroll()[i].getHauteur(); ++y)
            {
                if(vag.getTabTroll()[i].enVie())
                {
                    win.print(x,y,'T');
                }
            }
        }
    }

    for(int i=0; i<vag.getNbGobelin(); i++)
    {
        for(unsigned int x=vag.getTabGobelin()[i].getX(); x<vag.getTabGobelin()[i].getX()+vag.getTabGobelin()[i].getLargeur(); ++x)
        {
            for(unsigned int y=vag.getTabGobelin()[i].getY(); y<vag.getTabGobelin()[i].getY()+vag.getTabGobelin()[i].getHauteur(); ++y)
            {
                if(vag.getTabGobelin()[i].enVie())
                {
                    win.print(x,y,'G');
                }
            }
        }
    }

    if (her.getFleche() != NULL)
	{
		for(unsigned int x=(her.getFleche())->getX(); x<(her.getFleche())->getX()+(her.getFleche())->getDimx(); ++x)
    	{
        	for(unsigned int y=(her.getFleche())->getY(); y<(her.getFleche())->getY()+(her.getFleche())->getDimy();++y)
        	{
                if((her.getFleche())->getEnMouvement()) win.print(x,y,'f');
        	}
    	}
	}

	win.draw();
}

void txtBoucle (Jeu & jeu) {
	// Creation d'une nouvelle fenetre en mode texte
	// => fenetre de dimension et position (WIDTH,HEIGHT,STARTX,STARTY)
    WinTXT win (jeu.getConstTerrain().getDimX(),jeu.getConstTerrain().getDimY());

	bool ok = true;
	int c;

	do {
	    txtAff(win,jeu);

        #ifdef _WIN32
        Sleep(100);
		#else
		usleep(100000);
        #endif // WIN32

		jeu.actionsAutomatiques();

		c = win.getCh();
		switch (c) {
			case 's': //touche 'q'
				jeu.actionClavier('g'); //'g' comme "gauche"
				break;
			case 'f':
				jeu.actionClavier('d');
				break;
			case 'd':
				jeu.actionClavier('h');
				break;
			case 'e':
				jeu.actionClavier('b');
				break;
			case ' ': //touche "espace"
				jeu.actionClavier('e'); //'e' comme "épée"
				break;

			// cas z à supprimer ?
            case 'z': //touche "espace"
				jeu.actionClavier('z'); //'e' comme "épée"
				break;

			case 'v':
				jeu.actionClavier('a'); //'a' comme "arc"
				break;
			case 'y': //touche 'y'
				jeu.actionClavier('s'); //'s' comme "super-état"
				break;
			case 'p': //touche 'p'
				jeu.actionClavier('p'); //'p' comme "pause"
				break;
			case 'u': //touche 'u'
				jeu.actionClavier('t'); //'t' comme "tue-les tous"
				break;


			case 'q':
				ok = false;
				break;
		}

	} while (ok);

}
