//Ceci n'est pas un fichier à compiler. Il contient des bout de code qui peuvent être copiés puis collés ailleurs (puis adaptés) pour débuguer divers endroits du projet


#include <iostream>
#include <fstream>
#include <time.h>

using namespace std;

int main () {

    int variableTest = 1;
    float tempsDernierTraitement2 = 0;
    float tempsSeuilDeplacementTroll = 2/1000;

    clock_t t2 = clock();
	float tS2 = ((float)t2/CLOCKS_PER_SEC);
    if (tS2-tempsDernierTraitement2 > tempsSeuilDeplacementTroll) {


		// Coeur du code de débuguage, écriture dans un fichier pour voir ce qu'il se passe
        ofstream ResolutionAttaqueTroll;
	    ResolutionAttaqueTroll.open("Debug_et_tests/Attaque_troll.txt", ios::app);
	    if (ResolutionAttaqueTroll.is_open ()) {
		    ResolutionAttaqueTroll << "Le troll essaie d'attaquer pour la " << variableTest << "e fois à " << tS2*1000 << "s du début de l'exécution du programme" << endl;
		    ResolutionAttaqueTroll.close();
		    variableTest++;
	    }
	    else {
		    cout << "Impossible d'ouvrir le fichier" << endl;
	    }


        tempsDernierTraitement2 = tS2;
    }

    return 0;

}
