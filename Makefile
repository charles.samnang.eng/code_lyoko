EXEC_NAME_1 = bin/test
EXEC_NAME_2 = bin/codelyoko_txt
EXEC_NAME_3 = bin/codelyoko_sdl
OBJ_FILES = obj/Jeu.o obj/Vague.o obj/Terrain.o obj/Personnage.o obj/Heros.o obj/Projectile.o obj/Fleche.o obj/BouleDeFeu.o obj/Monstre.o obj/Troll.o obj/Gobelin.o obj/Tireur.o
OBJ_FILES_1 = obj/mainTest.o obj/txtJeu.o obj/winTxt.o $(OBJ_FILES)
OBJ_FILES_2 = obj/main_txt.o obj/txtJeu.o obj/winTxt.o $(OBJ_FILES)
OBJ_FILES_3 = obj/main_sdl.o obj/sdlJeu.o $(OBJ_FILES)
SRC_ONLY_CORE_FILES = src/core/Jeu.h src/core/Vague.h src/core/Terrain.h src/core/Personnage.h src/core/Heros.h src/core/Projectile.h src/core/Fleche.h src/core/BouleDeFeu.h src/core/Monstre.h src/core/Troll.h src/core/Gobelin.h src/core/Tireur.h
SRC_FILES_2 = src/txt/txtJeu.h src/txt/winTxt.h $(SRC_ONLY_CORE_FILES)
SRC_FILES_3 = src/sdl/sdlJeu.h $(SRC_ONLY_CORE_FILES)

CC = g++
CFLAGS = -Wall -ggdb

INCLUDE_DIR_SDL = -I/usr/include/SDL2
LIBS_SDL = -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer

# VALGRIND_MEMORY : plutôt "valgrind --leak-check=yes" ?
# VALGRIND_MEMORY et/ou VALGRIND_SDL : supprimer/modifier/ajouter des options ? notamment enlever --max-stackframe=2074024 ? "As discussed further in the description of --max-stackframe, a requirement for a large stack is a sign of potential portability problems. You are best advised to place all large data in heap-allocated memory" (https://www.valgrind.org/docs/manual/manual-core.html)
# --max-stackframe=2074024 : valeur peut-être à changer (par exemple si jamais il y a de nouveau un message disant de le faire quand on exécute "make valgrindtxt")
# VALGRIND_SDL : plutôt "valgrind --leak-check=full --num-callers=50 --suppressions=./valgrind_lif7.supp --show-reachable=yes -v" ?
VALGRIND_MEMORY = valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all --max-stackframe=2074024 --track-origins=yes --error-limit=no -s
VALGRIND_SDL = valgrind --leak-check=full --num-callers=50 --show-reachable=yes -v

all: $(EXEC_NAME_1) $(EXEC_NAME_2) $(EXEC_NAME_3)

#((Besoin d'ajouter $(INCLUDE_DIR_SDL) (diapo 8 CM3) à la commande exécutée ?))
$(EXEC_NAME_1) : $(OBJ_FILES_1)
	$(CC) $(CFLAGS) $(OBJ_FILES_1) -o $(EXEC_NAME_1) $(LIBS_SDL)

#((Besoin d'ajouter $(INCLUDE_DIR_SDL) (diapo 8 CM3) à la commande exécutée ?))
$(EXEC_NAME_2) : $(OBJ_FILES_2)
	$(CC) $(CFLAGS) $(OBJ_FILES_2) -o $(EXEC_NAME_2)

#((Besoin d'ajouter $(INCLUDE_DIR_SDL) (diapo 8 CM3) à la commande exécutée ?))
$(EXEC_NAME_3) : $(OBJ_FILES_3)
	$(CC) $(CFLAGS) $(OBJ_FILES_3) -o $(EXEC_NAME_3) $(LIBS_SDL)



obj/mainTest.o: src/mainTest.cpp src/sdl/sdlJeu.h $(SRC_FILES_2)
	$(CC) $(CFLAGS) $(INCLUDE_DIR_SDL) -c src/mainTest.cpp -o obj/mainTest.o

obj/main_txt.o: src/txt/main_txt.cpp $(SRC_FILES_2)
	$(CC) $(CFLAGS) -c src/txt/main_txt.cpp -o obj/main_txt.o

obj/main_sdl.o: src/sdl/main_sdl.cpp $(SRC_FILES_3)
	$(CC) $(CFLAGS) $(INCLUDE_DIR_SDL) -c src/sdl/main_sdl.cpp -o obj/main_sdl.o



obj/txtJeu.o: src/txt/txtJeu.cpp $(SRC_FILES_2)
	$(CC) $(CFLAGS) -c src/txt/txtJeu.cpp -o obj/txtJeu.o

obj/winTxt.o: src/txt/winTxt.cpp src/txt/winTxt.h
	$(CC) $(CFLAGS) -c src/txt/winTxt.cpp -o obj/winTxt.o

obj/sdlJeu.o: src/sdl/sdlJeu.cpp $(SRC_FILES_3)
	$(CC) $(CFLAGS) $(INCLUDE_DIR_SDL) -c src/sdl/sdlJeu.cpp -o obj/sdlJeu.o

obj/Jeu.o: src/core/Jeu.cpp $(SRC_ONLY_CORE_FILES)
	$(CC) $(CFLAGS) -c src/core/Jeu.cpp -o obj/Jeu.o

obj/Vague.o: src/core/Vague.cpp $(SRC_ONLY_CORE_FILES)
	$(CC) $(CFLAGS) -c src/core/Vague.cpp -o obj/Vague.o

obj/Terrain.o: src/core/Terrain.cpp src/core/Terrain.h
	$(CC) $(CFLAGS) -c src/core/Terrain.cpp -o obj/Terrain.o

obj/Personnage.o: src/core/Personnage.cpp src/core/Personnage.h
	$(CC) $(CFLAGS) -c src/core/Personnage.cpp -o obj/Personnage.o

obj/Heros.o: src/core/Heros.cpp $(SRC_ONLY_CORE_FILES)
	$(CC) $(CFLAGS) -c src/core/Heros.cpp -o obj/Heros.o

obj/Projectile.o: src/core/Projectile.cpp src/core/Projectile.h src/core/Terrain.h
	$(CC) $(CFLAGS) -c src/core/Projectile.cpp -o obj/Projectile.o

obj/Fleche.o: src/core/Fleche.cpp src/core/Fleche.h src/core/Terrain.h src/core/Projectile.h
	$(CC) $(CFLAGS) -c src/core/Fleche.cpp -o obj/Fleche.o

obj/BouleDeFeu.o: src/core/BouleDeFeu.cpp src/core/BouleDeFeu.h src/core/Terrain.h src/core/Projectile.h
	$(CC) $(CFLAGS) -c src/core/BouleDeFeu.cpp -o obj/BouleDeFeu.o

obj/Monstre.o: src/core/Monstre.cpp $(SRC_ONLY_CORE_FILES)
	$(CC) $(CFLAGS) -c src/core/Monstre.cpp -o obj/Monstre.o

obj/Troll.o: src/core/Troll.cpp $(SRC_ONLY_CORE_FILES)
	$(CC) $(CFLAGS) -c src/core/Troll.cpp -o obj/Troll.o

obj/Gobelin.o: src/core/Gobelin.cpp $(SRC_ONLY_CORE_FILES)
	$(CC) $(CFLAGS) -c src/core/Gobelin.cpp -o obj/Gobelin.o

obj/Tireur.o: src/core/Tireur.cpp $(SRC_ONLY_CORE_FILES)
	$(CC) $(CFLAGS) -c src/core/Tireur.cpp -o obj/Tireur.o


# !!! valgrindtest, valgrindtxt et valgrindsdl : BIEN AVOIR DEJA FAIT "make" SIMPLE AVANT (car le test valgrind se fait sur l'exécutable) !!!
# valgrindtest, valgrindtxt et valgrindsdl : mettre VALGRIND_MEMORY ou VALGRIND_SDL (pour chacun) ?
valgrindtest:
	$(VALGRIND_MEMORY) bin/test

valgrindtxt:
	$(VALGRIND_MEMORY) bin/codelyoko_txt

valgrindsdl:
	$(VALGRIND_SDL) bin/codelyoko_sdl


clean:
	rm obj/*.o
	rm bin/*

docu:
	doxygen doc/code_lyoko.doxy