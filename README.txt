AUTEURS :

    ENG Charles p1403762
    FERRER Raphaël p1908300
    GERMAIN Hugo p1908903 (!!! dernier commit le 11 mars 2021, et il a annoncé son départ du groupe le 29 mars 2021 !!!)

ENSEIGNANT REFERENT :

    FAVETTA Franck


IDENTIFIANT DU PROJET SUR LA FORGE : 18382

______________________________________

!!!!! Selon que vous voulez compiler le jeu pour l'exécuter ensuite en mode texte ou en mode SDL : inversez les noms des fichiers Terrain.cpp et Terrain2.cpp si le fichier qui s'appelle Terrain.cpp (qui est celui qui sera compilé) n'est pas celui qui correspond au mode que vous voulez (c'est indiqué au tout début du fichier s'il s'agit de la version pour le mode texte ou le mode SDL). Pour que ce changement soit bien pris en compte lors de la compilation, il faut ensuite que vous fassiez une modification mineure (ajoutez par exemple un commentaire vide) dans Terrain.cpp !!!!!

______________________________________

PROJET CODE LYOKO

    Dépôt contenant le code (et autres fichiers divers) d'un petit jeu vidéo en C++, réalisé dans le cadre du cours "Conception et Développement d'Applications en C++" de la L2 Informatique de l'Université Lyon 1.

______________________________________

ORGANISATION DES FICHIERS/REPERTOIRES :

    - bin : contient les fichiers (sans extension) exécutables compilés ;

    - Debug_et_tests : contient divers fichiers que nous avons créés et qui nous ont servi à débuguer ;

    - data : contient les images et les fichiers audio ;

    - doc : contient le diagramme de Gantt aux formats DOCX et PDF, le diagramme des classes aux formats PNG et DIA (!!! la version nommée "UML_Visual_Studio.png" est toujours la PLUS à jour, et la version nommée "UML.png" est toujours la MOINS à jour !!!), et potentiellement la documentation Doxygen du code (le fichier doxygen configuré qui permet de générer la documentation est doc/code_lyoko.doxy, et la page principale de la documentation [qui existe une fois make docu exécuté dans le terminal] est doc/html/index.html) ;

    - obj : contient les compilations intermédiaires (fichiers objets .o) ;

    - src : contient les fichiers sources .cpp et les fichiers headers .h ;

    - Cahier_des_charges_LIFAP4_1.docx et Cahier_des_charges_LIFAP4_1.pdf: le cahier des charges de notre projet, aux formats DOCX et PDF ;

    - IDEES_AMELIORATIONS.txt : fichier pas forcément à jour, qui sert aux développeurs à rassembler et partager leurs idées d'améliorations sur le jeu ;

    - Makefile : permet de compiler, tester les 3 exécutables avec Valgrind, nettoyer et générer la documentation Doxygen du code ;

    - Presentation_3.pptx et Presentation_3.pdf: le support de la présentation orale de notre projet, aux formats PPTX et PDF ;

    - RDV_avec_prof_16-03--par_Raphael.txt et RDV_avec_prof_1er_avril--par_Raphael : compte-rendus (réalisés par l'un des développeurs) d'une longue audioconférence avec notre enseignant référent et entre développeurs ;

    - Readme.txt : ce readme.


RÔLE DE CHAQUE EXECUTABLE :

    - bin/test : appelle testRegression (test de non-régression qui vérifie tout) et affiche les tests effectués à l'écran (i.e. exécute mainTest.cpp) ;
    - bin/codelyoko_txt : jeu en mode texte (i.e. exécute main_txt.cpp) ;
    - bin/codelyoko_sdl : jeu en mode SDL2 (i.e. exécute main_sdl.cpp) ;


POUR INSTALLER LES LOGICIELS NECESSAIRES :

    Sous Linux, exécutez la commande suivante dans le terminal de commande :

	sudo apt install valgrind doxygen libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libsdl2-mixer-dev imagemagick

______________________________________

PLACEZ-VOUS A LA RACINE DU PROJET DEPUIS UN TERMINAL DE COMMANDE LINUX, PUIS :

    - POUR COMPILER : Exécutez la commande make.

    - POUR EXECUTER : Exécutez la commande bin/test, bin/codelyoko_txt ou bin/codelyoko_sdl, selon le programme que vous voulez exécuter.

    - POUR TESTER AVEC VALGRIND : Pour tester bin/test, bin/codelyoko_txt ou bin/codelyoko_sdl avec Valgrind, exécutez respectivement make valgrindtest, make valgrindtxt ou valgrindsdl.

    - POUR NETTOYER : Pour vider les dossiers bin et obj, exécutez make clean.

    - POUR GENERER LA DOCUMENTATION : Exécutez make docu.

______________________________________

BUT DU JEU :

    Venez à bout de toutes les vagues de monstres, pour devenir le héros de l'arène !


PARAMETRES (INFLUENT SUR LE CONFORT ET LA DIFFICULTE DU JEU) :

    Sous Linux Ubuntu 20.04.2 LTS GNOME 3.36.8, allez dans Paramètres -> Accès universel -> Touches de répétitions (qui doit être activé), puis mettez le délai au minimum. Mettez la vitesse entre 1/4 et 3/4. Moins la vitesse est élevée, plus le jeu sera difficile ; mais la vitesse la plus adaptée au jeu est environ 1/2.


COMMANDES DU JEU :

    - Déplacer le héros et viser : ESDF
    - Donner un coup d'épée : Espace
    - Tirer une flèche : V
    - [fonctionnalité potentiellement non testée de manière complète, non équilibrée et non visible pour l'utilisateur] Passer en "super-état" (une fois la jauge pleine et pour une durée limitée): Y : la "jauge" (potentiellement invisible pour l'utilisateur) de "super-état" se remplit de 2 points si le joueur touche un ennemi avec une flèche, et de 5 points s'il touche un ennemi avec son épée. Elle se remplit au maximum jusqu'à 100 points. Une fois qu'elle est entièrement remplie, l'utilisateur peut appuyer sur la touche Y pour activer son "super-état", et ainsi multiplier ses dégâts à l'arc et à l'épée par 2. Au bout de 100 ticks, la "jauge" est entièrement vidée et l'utilisateur repasse en état normal. Toucher des monstres avec une flèche ou son épée lui rapporte alors à nouveau des points, etc.
    - Mettre en pause : P
